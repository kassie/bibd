package by.bsuir.bibd.service;

import by.bsuir.bibd.model.purchase.Purchase;
import by.bsuir.bibd.model.purchase.PurchaseService;
import by.bsuir.bibd.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PurhcaseInfoInterceptor extends HandlerInterceptorAdapter {

    private static final int MAX_NEW_PURCHASES = 3;

    @Autowired
    private PurchaseService purchaseService;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if(modelAndView != null) {
            List<Purchase> newPurchases = purchaseService.findByState(Purchase.State.NEW);
            int totalNewPurchases = newPurchases.size();
            if(totalNewPurchases > MAX_NEW_PURCHASES) {
                newPurchases = newPurchases.subList(0, MAX_NEW_PURCHASES);
            }
            modelAndView.addObject("newPurchases", newPurchases);
            modelAndView.addObject("totalNewPurchases", totalNewPurchases);
        }
    }
}
