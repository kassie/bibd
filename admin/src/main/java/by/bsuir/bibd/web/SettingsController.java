package by.bsuir.bibd.web;

import by.bsuir.bibd.model.currency.Currency;
import by.bsuir.bibd.model.currency.CurrencyService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class SettingsController {

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String showSettings(Model model) {
        model.addAttribute("currencies", currencyService.findAll());
        return "admin:settings/show";
    }

    @RequestMapping(value = "/settings/currencies/add", method = RequestMethod.GET)
    public String showCurrencyForm(Model model) {
        model.addAttribute("currency", new Currency());
        return "admin:settings/currency-form";
    }

    @RequestMapping(value = "/settings/currencies/add", method = RequestMethod.POST)
    public String addCurrency(Model model, @ModelAttribute @Valid Currency currency,
            BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("currency", currency);
            return "admin:settings/currency-form";
        }
        currencyService.create(currency);
        return "redirect:/settings";
    }

    @RequestMapping(value = "/settings/currencies/{id}", method = RequestMethod.GET)
    public String editCurrency(Model model, @PathVariable Long id) {
        Currency currency = currencyService.findOne(id);
        if(currency == null) {
            return "errors/404";
        }
        model.addAttribute("currency", currency);
        return "admin:settings/currency-form";
    }

    @RequestMapping(value = "/settings/currencies/update", method = RequestMethod.POST)
    public String updateCurrency(Model model, @ModelAttribute @Valid Currency currency,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("currency", currency);
            return "admin:settings/currency-form";
        }
        FlashMessage result = currencyService.update(currency);
        redirectAttributes.addFlashAttribute(result.getKey(), result.getCode());
        return "redirect:/settings";
    }

    @RequestMapping(value = "/settings/currencies/delete", method = RequestMethod.POST)
    @ResponseBody
    public FlashMessage delete(@RequestParam Long id, HttpServletRequest req) {
        FlashMessage result = currencyService.delete(id);
        result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
        return result;
    }
}
