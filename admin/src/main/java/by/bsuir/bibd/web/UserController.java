package by.bsuir.bibd.web;

import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.model.user.UserService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String listUsers(Model model, @RequestParam(required = false) Long id) {
        model.addAttribute("userId", id);
        model.addAttribute("users", userService.findAll());
        model.addAttribute("roles", User.Role.getList());
        return "admin:user/list";
    }

    @RequestMapping(value = "/users/change-role", method = RequestMethod.POST)
    @ResponseBody
    public FlashMessage changeRole(@RequestParam("userId") Long id, @RequestParam("role") String newRole,
            HttpServletRequest req) {
        User user = userService.findOne(id);
        FlashMessage result = FlashMessage.success("user.updated");
        result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
        if(user == null) {
            result = FlashMessage.error("user.error.not-found");
            result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
            return result;
        }
        try {
            User.Role role = User.Role.valueOf(newRole);
            user.setRole(role);
            result = userService.update(user);
            result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
            return result;
        } catch (IllegalArgumentException e) {
            result = FlashMessage.error("user.role.error.not-found");
            result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
            return result;
        }
    }

    @RequestMapping(value = "/users/delete", method = RequestMethod.POST)
    @ResponseBody
    public FlashMessage deleteUser(@RequestParam Long id, HttpServletRequest req) {
        User user = userService.findOne(id);
        FlashMessage result = FlashMessage.success("user.deleted");
        if(user == null) {
            result = FlashMessage.error("user.error.not-found");
        }
        userService.delete(id);
        result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
        return result;
    }
}
