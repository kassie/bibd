package by.bsuir.bibd.web;

import by.bsuir.bibd.model.category.CategoryService;
import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Controller
public class ProductController {

    @Value("${product.admin.items-on-page}")
    private Integer itemsOnPage;

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String showProducts(Model model, @RequestParam(required = false) Integer page) {
        model.addAttribute("page", productService.findAll(page, itemsOnPage));
        return "admin:product/list";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.GET)
    public String addProduct(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("categories", categoryService.findAll());
        return "admin:product/form";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute @Valid Product product, BindingResult bindingResult, Model model,
            @RequestParam("categoryId") Long categoryId, @RequestParam MultipartFile image) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("product", product);
            model.addAttribute("categories", categoryService.findAll());
            return "admin:product/form";
        }
        product.setActive(true);
        product.setCategory(categoryService.findOne(categoryId));
        product.setPhoto(fileService.uploadFile(image));
        productService.create(product);
        return "redirect:/products";
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public String updateProduct(Model model, @PathVariable Long id) {
        Product product = productService.findOne(id);
        if(product == null) {
            return "admin:error/404";
        }
        model.addAttribute("product", product);
        model.addAttribute("categories", categoryService.findAll());
        return "admin:product/form";
    }

    @RequestMapping(value = "/products/update", method = RequestMethod.POST)
    public String saveUpdates(Model model, @ModelAttribute @Valid Product product, BindingResult bindingResult,
            @RequestParam(required = false) MultipartFile image, @RequestParam("categoryId") Long categoryId) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("product", product);
            model.addAttribute("categories", categoryService.findAll());
            return "admin:product/form";
        }
        productService.update(product, image, categoryService.findOne(categoryId));
        return "redirect:/products";
    }

    @RequestMapping(value = "/products/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteProduct(@RequestParam Long id) {
        fileService.delete(productService.findOne(id).getPhoto());
        productService.delete(id);
        return "success";
    }
}
