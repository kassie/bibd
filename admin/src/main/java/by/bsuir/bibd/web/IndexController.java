package by.bsuir.bibd.web;

import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.model.purchase.Purchase;
import by.bsuir.bibd.model.purchase.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class IndexController {

    private static final int MAX_PURCHASES = 15;

    @Autowired
    private ProductService productService;

    @Autowired
    private PurchaseService purchaseService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showIndex(Model model) {
        model.addAttribute("totalProducts", productService.findAll().size());
        List<Purchase> purchaseList = purchaseService.findAll();
        model.addAttribute("totalPurchases", purchaseList.size());
        if(purchaseList.size() > MAX_PURCHASES) {
            purchaseList = purchaseList.subList(0, MAX_PURCHASES);
        }
        model.addAttribute("purchases", purchaseList);
        return "admin:admin/index";
    }
}
