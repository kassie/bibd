package by.bsuir.bibd.web;

import by.bsuir.bibd.model.purchase.Purchase;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItemService;
import by.bsuir.bibd.model.purchase.PurchaseService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PurchaseItemService purchaseItemService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    public String showPurchases(Model model, @RequestParam(required = false) Integer page,
            @RequestParam(value = "state", required = false) String state) {
        Purchase.State resultState = Purchase.State.NEW;
        List<String> stateList = Purchase.State.getStateList();
        if(state != null) {
            if(!stateList.contains(state)) {
                return "admin:error/404";
            }
            resultState = Purchase.State.valueOf(state.toUpperCase());
        }
        model.addAttribute("stateList", stateList);
        model.addAttribute("page", purchaseService.findByState(resultState, page));
        return "admin:purchase/list";
    }

    @RequestMapping(value = "/purchases/{id}", method = RequestMethod.GET)
    public String showPurchaseDetails(Model model, @PathVariable Long id) {
        Purchase purchase = purchaseService.findOne(id);
        if(purchase == null) {
            return "admid:error/404";
        }
        model.addAttribute("stateList", Purchase.State.getStateList());
        model.addAttribute("purchase", purchase);
        model.addAttribute("items", purchaseItemService.findByPurchaseId(purchase.getId()));
        return "admin:purchase/show";
    }

    @RequestMapping(value = "/purchases/{id}/change-state", method = RequestMethod.POST)
    @ResponseBody
    public FlashMessage changeState(@PathVariable Long id, @RequestParam("state") String state,
            HttpServletRequest req) {
        FlashMessage result;
        Purchase purchase = purchaseService.findOne(id);
        if(purchase != null && Purchase.State.getStateList().contains(state)) {
            purchase.setState(Purchase.State.valueOf(state.toUpperCase()));
            result = purchaseService.update(purchase);
        } else {
            result = FlashMessage.error("purchase.error.invalid-state");
        }
        result.setMessage(messageSource.getMessage(result.getCode(), null, RequestContextUtils.getLocale(req)));
        return result;
    }

    @RequestMapping(value = "/purchases/remove", method = RequestMethod.DELETE)
    public String removePurchase(RedirectAttributes redirectAttributes, @RequestParam Long purchaseId) {
        FlashMessage result = purchaseService.delete(purchaseId);
        redirectAttributes.addFlashAttribute(result.getKey(), result.getCode());
        return "redirect:/purchases";
    }

}
