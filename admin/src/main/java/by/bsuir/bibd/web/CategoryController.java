package by.bsuir.bibd.web;

import by.bsuir.bibd.model.category.Category;
import by.bsuir.bibd.model.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Value("${category.admin.items-on-page}")
    private Integer itemsOnPage;

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String showList(Model model, @RequestParam(required = false) Integer page) {
        model.addAttribute("page", categoryService.findAll(page, itemsOnPage));
        return "admin:category/list";
    }

    @RequestMapping(value = "/categories/add", method = RequestMethod.GET)
    public String addCategoryt(Model model) {
        model.addAttribute("category", new Category());
        return "admin:category/form";
    }

    @RequestMapping(value = "/categories/add", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute @Valid Category category, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("category", category);
            return "admin:category/form";
        }
        categoryService.create(category);
        return "redirect:/categories";
    }

    @RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
    public String updateProduct(Model model, @PathVariable Long id) {
        Category category = categoryService.findOne(id);
        if(category == null) {
            return "admin:error/404";
        }
        model.addAttribute("category", category);
        return "admin:category/form";
    }

    @RequestMapping(value = "/categories/update", method = RequestMethod.POST)
    public String saveUpdates(Model model, @ModelAttribute @Valid Category category, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("category", category);
            return "admin:category/form";
        }
        categoryService.update(category);
        return "redirect:/categories";
    }

    @RequestMapping(value = "/categories/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteCategory(@RequestParam Long id) {
        categoryService.delete(id);
        return "success";
    }
}
