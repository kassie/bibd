package by.bsuir.bibd.web;

import by.bsuir.bibd.model.currency.CurrencyService;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.service.export.ProductExportService;
import by.bsuir.bibd.service.importservice.ImportReport;
import by.bsuir.bibd.service.importservice.ImportService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Controller
public class ImportExportController {

    private static final Logger logger = Logger.getLogger(ImportExportController.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private ImportService importService;

    @RequestMapping(value = "/products/export", method = RequestMethod.GET)
    public void export(HttpServletResponse response) {
        try(OutputStream os = response.getOutputStream()) {
            response.setContentType("text/csv;charset=UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=doc.csv");
            new ProductExportService(productService.getProductsForExport(), currencyService.findMain()).export(os);
            os.flush();
        } catch (IOException e) {
            logger.error(e);
        }
    }

    @RequestMapping(value = "/products/import", method = RequestMethod.GET)
    public String showImportProductsForm() {
        return "admin:import/show";
    }

    @RequestMapping(value = "/products/import", method = RequestMethod.POST)
    public String importProducts(@RequestParam MultipartFile file, Model model) {
        ImportReport report = new ImportReport();
        if(file != null) {
            report = importService.importFromCsv(file);
        } else {
            report.addError("File not found", 0, null);
        }
        model.addAttribute("report", report);
        return "admin:import/report";
    }
}
