function getQueryVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}

$(document).on('click', '.prevent-default', function(e) {
    e.preventDefault();
});

//flash message
var flashMessageSelector = '.flash-message';
var flashErrorSelector = '.flash-error';
var flashSuccessSelector = '.flash-success';
$(document).on('click', flashMessageSelector, function() {
    $(this).fadeOut(200);
});

var FLASH_MESSAGE = {
    success : 'flashSuccessMessage',
    error : 'flashErrorMessage'
};

function displayFlashMessage(message) {
    if(message) {
        if(message.key == FLASH_MESSAGE.success) {
            if(!message.message) { message.message = "Done"; }
            $(flashSuccessSelector).html('<span>' + message.message + '</span>');
            $(flashSuccessSelector).fadeIn(200);
        } else if(message.key == FLASH_MESSAGE.error) {
            if(!message.message) { message.message = "Error"; }
            $(flashErrorSelector).html('<span>' + message.message + '</span>');
            $(flashErrorSelector).fadeIn(200);
        }
        setTimeout(function() {
                $(flashMessageSelector).fadeOut(400);
            },
            3500
        )
    }
}
