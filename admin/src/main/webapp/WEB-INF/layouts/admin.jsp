<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="eshop_admin" />
    <meta name="author" content="Leonid Sazankov" />

    <title><spring:message code="app.title" /></title>

    <link type="image/x-icon" rel="shortcut icon" href="/favicon.ico" />

    <!-- Bootstrap core CSS -->
    <link href="${home}/resources/assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Custom CSS here -->
    <link href="${home}/resources/assets/css/sb-admin.css" rel="stylesheet" />
    <link href="${home}/resources/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- JavaScript -->
    <script src="${home}/resources/assets/js/jquery-1.10.2.js"></script>
    <script src="${home}/resources/assets/js/bootstrap.js"></script>
    <script src="${home}/resources/assets/js/base.js"></script>

    <!-- page css -->
    <%--<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">--%>
</head>

<body>
<div id="wrapper">
    <tiles:insertAttribute name="header" />
    <div id="page-wrapper">
        <tiles:insertAttribute name="body" />
    </div><!-- /#page-wrapper -->
</div><!-- /#wrapper -->

<%@ include file="/WEB-INF/views/utils/_flash-message.jsp"  %>

<!-- Page Specific Plugins -->
<%--<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>--%>
<%--<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>--%>
<%--<script src="${home}/resources/assets/js/morris/chart-data-morris.js"></script>--%>
<%--<script src="${home}/resources/assets/js/tablesorter/jquery.tablesorter.js"></script>--%>
<%--<script src="${home}/resources/assets/js/tablesorter/tables.js"></script>--%>

</body>
</html>
