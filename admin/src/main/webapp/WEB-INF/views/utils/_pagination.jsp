<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<c:set value="4" var="limit"/>
<c:set value="${page.totalPages}" var="total" />
<c:set value="${page.number + 1}" var="cur" />
<c:if test="${total > 1}">
    <ul class="pagination">
        <c:if test="${cur != 1}">
            <li><a href="${target}?page=${cur - 2}">&laquo;</a></li>
        </c:if>
        <c:set value="${cur - limit}" var="begin"/>
        <c:set value="${cur + limit}" var="end"/>
        <c:choose>
            <c:when test="${limit * 2 + 1 < total}">
                <c:if test="${cur - limit <= 0}">
                    <c:set var="begin" value="1"/>
                    <c:set var="end" value="${limit * 2 + 1}"/>
                </c:if>
                <c:if test="${cur + limit > total}">
                    <c:set var="begin" value="${total - limit * 2}"/>
                    <c:set var="end" value="${total}"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:set value="1" var="begin"/>
                <c:set value="${total}" var="end"/>
            </c:otherwise>
        </c:choose>
        <c:forEach var="i" begin="${begin}" end="${end}">
            <c:choose>
                <c:when test="${cur == i}">
                    <li class="active"><a>${i}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="${target}?page=${i - 1}">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${cur != total}">
            <li><a href="${target}?page=${cur + 1}">&raquo;</a></li>
        </c:if>
    </ul>
</c:if>