<%@ include file="/WEB-INF/views/utils/include.jsp"%>

<%--<c:set var="success" value="${flashSuccessMessage}" />--%>
<%--<c:set var="error" value="${flashErrorMessage}" />--%>

    <div class="flash-message flash-success">
<c:if test="${not empty flashSuccessMessage}">
        <span>
            <spring:message code="${flashSuccessMessage}" />
        </span>
</c:if>
    </div>

    <div class="flash-message flash-error">
<c:if test="${not empty flashErrorMessage}" >
        <span>
            <spring:message code="${flashErrorMessage}" />
        </span>
</c:if>
    </div>
