<%@ include file="/WEB-INF/views/utils/include.jsp"%>

<c:set var="update" value="${false}" />
<c:set var="action" value="${home}/categories/add" />
<spring:message code="category.add" var="headers"/>
<spring:message code="add" var="button" />
<c:if test="${not empty category.id}">
    <c:set var="update" value="${true}" />
    <c:set var="action" value="${home}/categories/update" />
    <c:set var="headers" value="${category.title}" />
    <spring:message code="update" var="button" />
</c:if>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">${headers}</h1>
        <form:form
                commandName="category"
                cssClass="form-horizontal"
                action="${action}"
                method="POST"
                enctype="multipart/form-data">
            <div class="form-group">
                <spring:message code="title" var="title"/>
                <form:label cssClass="control-label col-md-2" path="title">${title}</form:label>
                <div class="col-md-6">
                    <form:input path="title" cssClass="form-control" placeholder="${title}" value="${category.title}"/>
                    <form:errors path="title" cssClass="form-error"/>
                </div>
            </div>
            <div class="form-group">
                <spring:message code="description" var="description"/>
                <form:label path="description" cssClass="control-label col-md-2">${description}</form:label>
                <div class="col-md-6">
                    <form:textarea path="description" cssClass="form-control" placeholder="${description}" />
                    <form:errors path="description" cssClass="form-error" />
                </div>
            </div>
            <div class="form-group">
                <spring:message code="category.link" var="link"/>
                <form:label cssClass="control-label col-md-2" path="link">${link}</form:label>
                <div class="col-md-6">
                    <form:input path="link" cssClass="form-control" placeholder="${link}" value="${category.link}"/>
                    <form:errors path="link" cssClass="form-error"/>
                </div>
            </div>
            <c:if test="${update}">
                <form:hidden path="id" />
            </c:if>
            <div class="form-group">
                <div class="col-md-2 col-md-offset-2">
                    <input type="submit" class="btn btn-success" value="${button}"/>
                </div>
            </div>
        </form:form>
    </div>
</div>
