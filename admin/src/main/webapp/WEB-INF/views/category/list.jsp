<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<script type="text/javascript">
    $(document).ready(function() {
        $('.delete-mark').click(function() {
            var $tr = $(this).parents('tr');
            var catId = $(this).data('categoryId');
            $.post('${home}/categories/delete', {id : catId}, function(data) {
                if(data) {
                    $tr.remove();
                }
            });
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="category.plural"/>
            <span class="pull-right">
                <a href="${home}/categories/add" class="btn btn-success"><spring:message code="add" /></a>
            </span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li class="active"><i class="fa fa-dashboard"></i> <spring:message code="category.plural" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th><spring:message code="title"/></th>
                    <th><spring:message code="description"/></th>
                    <th><spring:message code="category.link"/></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${page.content}" var="category" varStatus="counter">
                        <tr>
                            <td class="td-num">${counter.count}</td>
                            <td><a href="${home}/categories/${category.id}">${category.title}</a></td>
                            <td>${category.description}</td>
                            <td>${category.link}</td>
                            <td class="td-num"><a href="${home}/categories/${category.id}" class="btn btn-default">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a></td>
                            <td class="td-num"><button class="btn btn-danger delete-mark"  data-category-id="${category.id}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div><!-- /.row -->