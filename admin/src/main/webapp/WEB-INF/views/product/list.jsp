<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<script type="text/javascript">
    $(document).ready(function() {
        $('.delete-mark').click(function() {
            var $tr = $(this).parents('tr');
            var prodId = $(this).data('productId');
            $.post('${home}/products/delete', {id : prodId}, function(data) {
                if(data) {
                    $tr.remove();
                }
            });
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="product.plural"/>
            <span class="pull-right">
                <a href="${home}/products/export" class="btn btn-primary"><spring:message code="export" /></a>
                <a href="${home}/products/import" class="btn btn-primary"><spring:message code="import" /></a>
                <a href="${home}/products/add" class="btn btn-success"><spring:message code="add" /></a>
            </span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li class="active"><i class="fa fa-dashboard"></i> <spring:message code="product.plural" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th><spring:message code="title" /></th>
                    <th><spring:message code="product.articul" /></th>
                    <th><spring:message code="price" /></th>
                    <th><spring:message code="date" /></th>
                    <th><spring:message code="category" /></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${page.content}" var="product" varStatus="counter">
                        <tr>
                            <td class="td-num">${counter.count}</td>
                            <td><a href="${home}/products/${product.id}">${product.title}</a></td>
                            <td>${product.articul}</td>
                            <td>$${product.price}</td>
                            <td><fmt:formatDate value="${product.created}" pattern="dd-MM-yyyy hh:mm:ss"/></td>
                            <td>${product.category.title}</td>
                            <td class="td-num"><a href="${home}/products/${product.id}" class="btn btn-default">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a></td>
                            <td class="td-num"><button class="btn btn-danger delete-mark"  data-product-id="${product.id}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div><!-- /.row -->