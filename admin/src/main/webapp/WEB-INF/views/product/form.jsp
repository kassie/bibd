<%@ include file="/WEB-INF/views/utils/include.jsp"%>

<c:set var="update" value="${false}" />
<c:set var="action" value="${home}/products/add" />
<spring:message code="product.add" var="headers"/>
<spring:message code="add" var="button" />
<c:if test="${not empty product.id}">
    <c:set var="update" value="${true}" />
    <c:set var="action" value="${home}/products/update" />
    <c:set var="headers" value="${product.title}" />
    <spring:message code="update" var="button" />
</c:if>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">${headers}</h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li><a href="${home}/products"><i class="fa fa-dashboard"></i>&nbsp;<spring:message code="product.plural" /></a></li>
            <li class="active">${product.title}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-7">
        <form:form
                commandName="product"
                cssClass="form-horizontal"
                action="${action}"
                method="POST"
                enctype="multipart/form-data">
            <div class="form-group">
                <spring:message code="title" var="title"/>
                <form:label cssClass="control-label col-md-2" path="title">${title}</form:label>
                <div class="col-md-10">
                    <form:input path="title" cssClass="form-control" placeholder="${title}" value="${product.title}"/>
                    <form:errors path="title" cssClass="form-error"/>
                </div>
            </div>
            <div class="form-group">
                <spring:message code="product.articul" var="articul"/>
                <form:label cssClass="control-label col-md-2" path="articul">${articul}</form:label>
                <div class="col-md-10">
                    <form:input path="articul" cssClass="form-control" placeholder="${articul}" value="${product.articul}"/>
                    <form:errors path="articul" cssClass="form-error"/>
                </div>
            </div>
            <div class="form-group">
                <spring:message code="price" var="price" />
                <form:label class="control-label col-md-2" path="price">${price}</form:label>
                <div class="col-md-10">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <form:input path="price" cssClass="form-control" placeholder="${price}" />
                        <form:errors path="price" cssClass="form-error" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="categorySelect" >
                    <spring:message code="category" />
                </label>
                <div class="col-md-10">
                    <select class="form-control" name="categoryId" id="categorySelect" >
                        <c:forEach items="${categories}" var="category">
                            <c:set var="selected" value="" />
                            <c:if test="${category.id == product.category.id}" >
                                <c:set var="selected" value="selected" />
                            </c:if>
                            <option ${selected}  value="${category.id}">${category.title}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <spring:message code="description" var="description"/>
                <form:label path="description" cssClass="control-label col-md-2">${description}</form:label>
                <div class="col-md-10">
                    <form:textarea path="description" cssClass="form-control" placeholder="${description}" rows="5" />
                    <form:errors path="description" cssClass="form-error" />
                </div>
            </div>
            <div class="form-group">
                <spring:message code="image" var="image"/>
                <label class="control-label col-md-2" for="imageInput">${image}</label>
                <div class="col-md-10">
                    <input type="file" name="image" id="imageInput" />
                </div>
            </div>
            <c:if test="${update}">
                <%--<c:if test="${not empty product.photo}">--%>
                    <%--<div class="form-group">--%>
                        <%--<div class="col-md-6 col-md-offset-2">--%>
                            <%--<img src="${product.photo}" class="img-thumbnail" />--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</c:if>--%>
                <div class="form-group">
                    <form:hidden path="id" />
                    <div class="col-md-2 col-md-offset-2">
                        <div class="checkbox">
                            <label path="active">
                                <form:checkbox path="active"/><spring:message code="active"/>
                            </label>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="form-group">
                <div class="col-md-2 col-md-offset-2">
                    <input type="submit" class="btn btn-success" value="${button}"/>
                </div>
            </div>
        </form:form>
    </div>
    <div class="col-md-5">
        <c:if test="${not empty product.photo}">
            <div class="form-group">
                <img src="${product.photo}" class="img-thumbnail" />
            </div>
        </c:if>
    </div>
</div>
