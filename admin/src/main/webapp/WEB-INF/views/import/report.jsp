<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="import.report"/>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li><a href="${home}/products"><spring:message code="product.plural" /></a></li>
            <li><a href="${home}/products/import"><spring:message code="import" /></a></li>
            <li class="active"><i class="fa fa-dashboard"></i> <spring:message code="import.report" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <c:choose>
            <c:when test="${fn:length(report.errors) > 0}">
                <spring:message code="import.report.has-errors" /> (<spring:message code="total" /> ${fn:length(report.errors)}) <br />
                <span><spring:message code="import.report.total" />: ${report.totalItems}</span>
                <span><spring:message code="import.report.imported" />: ${report.importedItems}</span>
                <table class="table table-hover">
                    <c:forEach items="${report.errors}" var="error" varStatus="counter">
                        <tr>
                            <td class="td-num">${counter.count}</td>
                            <td>${error}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <spring:message code="import.report.success" /><br />
                <span><spring:message code="import.report.total" />: ${report.totalItems}</span>
            </c:otherwise>
        </c:choose>
    </div>
</div><!-- /.row -->