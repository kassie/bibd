<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="import"/>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li><a href="${home}/"><spring:message code="product.plural" /></a></li>
            <li class="active"><i class="fa fa-dashboard"></i> <spring:message code="import" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <h4><spring:message code="import.prompt" /></h4>
        <form class="form-horizontal"
              action="${home}/products/import"
              method="post"
              enctype="multipart/form-data" >
            <div class="form-group">
                <div class="col-md-3">
                    <input type="file" name="file" accept=".csv"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <spring:message code="send" var="send" />
                    <input type="submit" value="${send}" class="btn btn-primary"/>
                </div>
            </div>
          </form>
    </div>
</div><!-- /.row -->