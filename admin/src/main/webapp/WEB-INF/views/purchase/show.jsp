<%@include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="purchase" />&nbsp;#${purchase.id}
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li><a href="${home}/purchases"><spring:message code="purchase.plural" /></a></li>
            <li class="active"><spring:message code="purchase" /> #${purchase.id}</li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2 class="panel-title"><spring:message code="purchase.info" />&nbsp;#${purchase.id}</h2>
                <span class="info-holder" data-purchase-id="${purchase.id}"></span>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="bold td-title"><spring:message code="date" /></td>
                        <td>${purchase.purchased}</td>
                    </tr>
                    <tr>
                        <td class="bold td-title"><spring:message code="total-price" /></td>
                        <td>$${purchase.total}</td>
                    </tr>
                    <tr>
                        <td class="bold td-title"><spring:message code="purchase.state" /></td>
                        <td>
                            <c:forEach items="${stateList}" var="state">
                                <button data-state="${state}" class="btn btn-default btn-purchase-state">
                                    <spring:message code="purchase.state.${state}" />
                                </button>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><spring:message code="product.plural" /></h3>
            </div>
                <table class="table table-hover panel-body">
                    <tr>
                        <th>#</th>
                        <th><spring:message code="title" /></th>
                        <th>$<spring:message code="price" /></th>
                    </tr>
                    <c:forEach items="${items}" var="item" varStatus="counter">
                        <tr>
                            <td>${counter.count}</td>
                            <td><a href="${home}/products/${item.productId}">${item.title}</a></td>
                            <td>${item.price}</td>
                        </tr>
                    </c:forEach>
                </table>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><spring:message code="customer.info" /></h3>
            </div>
            <table class="table table-hover panel-body">
                <c:set var="customer" value="${purchase.customer}" />
                <tr><td>${customer.name} ${customer.surname}</td></tr>
                <tr><td><a href="${home}/users?id=${customer.id}">${customer.email}</a></td></tr>
                <c:if test="${customer.phone != 0}"><tr><td>${customer.phone}</td></tr></c:if>
                <c:if test="${not empty customer.address}"><tr><td>${customer.address}</td></tr></c:if>
            </table>
            </div>
        </div>
    </div>
</div><!-- /.row -->

<script type="text/javascript">
    $(document).ready(function() {
        var $infoHolder = $('.info-holder');
        changeStateButton = ".btn-purchase-state";
        var state = '${fn:toLowerCase(purchase.state)}';
        changeState(state);

        $(changeStateButton).click(function() {
            var newState = $(this).data('state');
            var id = $infoHolder.data('purchaseId');
            $.post('${home}/purchases/' + id + '/change-state',
                {id : id, state : newState},
                function(message) {
                    if(message.key == FLASH_MESSAGE.success) {
                        changeState(newState);
                    }
                    displayFlashMessage(message);
            });
        });

    });

    function changeState(state) {
        if(state) {
            $(changeStateButton).removeClass('active');
            $(changeStateButton + '[data-state="' + state + '"]').addClass('active');
        }
    }
</script>