<%@include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="purchase.plural"/>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li class="active"><i class="fa fa-dashboard"></i> <spring:message code="purchase.plural" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-pills purchase-state">
            <c:forEach items="${stateList}" var="state">
                <li><a href="${home}/purchases?state=${state}"><spring:message code="purchase.state.${state}" /></a></li>
            </c:forEach>
        </ul>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th><spring:message code="customer" /></th>
                    <th><spring:message code="price" /></th>
                    <th><spring:message code="date" /></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${page.content}" var="purchase" varStatus="counter">
                    <tr>
                        <td class="td-num">${purchase.id}</td>
                        <td>${purchase.customer.email}</td>
                        <td>$${purchase.total}</td>
                        <td><fmt:formatDate value="${purchase.purchased}" pattern="dd-MM-yyyy hh:mm:ss"/></td>
                        <td class="td-num"><a href="${home}/purchases/${purchase.id}" class="btn btn-default">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a></td>
                        <td class="td-num">
                            <button class="btn btn-danger delete-purchase" data-toggle="modal" data-target="#confirm-delete" data-purchase-id="${purchase.id}">
                            <span class="glyphicon glyphicon-remove"></span>
                            </button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <%@include file="/WEB-INF/views/utils/_pagination.jsp" %>
        </div>
    </div>
</div><!-- /.row -->

<!-- Modal -->
<form class="modal fade"
        id="confirm-delete"
        tabindex="-1"
        role="dialog"
        aria-labelledby="confirm-deleteLabel"
        aria-hidden="true"
        method="post"
        action="${home}/purchases/remove">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirm-deleteLabel"><spring:message code="purchase.deletion" /></h4>
            </div>
            <div class="modal-body">
                <spring:message code="purchase.delete.confirmation" />
            </div>
            <div class="modal-footer">
                <input type="hidden" name="_method" value="delete">
                <input type="hidden" name="purchaseId" />
                <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="close" /></button>
                <input type="submit" class="btn btn-danger" value="<spring:message code='delete' />">
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var state = getQueryVariable('state');
        if(state) {
            var url = '${home}/purchases?state=' + state;
            $('.purchase-state a[href="' + url + '"]').parents('li').addClass('active');
        } else {
            $('.purchase-state a').first().parents('li').addClass('active');
        }

        $('.delete-purchase').click(function() {
            var id = $(this).data('purchaseId');
            $('input[name="purchaseId"]').val(id);
        });
    });
</script>