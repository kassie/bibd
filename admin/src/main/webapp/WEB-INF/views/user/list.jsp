<%@include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="user.plural" />
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li class="active"><spring:message code="user.plural" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
                <table class="table table-hover">
                    <tr>
                        <th><spring:message code="user.name" /></th>
                        <th><spring:message code="user.surname" /></th>
                        <th><spring:message code="email" /></th>
                        <th><spring:message code="user.address" /></th>
                        <th><spring:message code="currency" /></th>
                        <th><spring:message code="user.role" /></th>
                        <th></th>
                    </tr>
                    <c:forEach items="${users}" var="user" varStatus="counter">
                        <tr>
                            <td>${user.name}</td>
                            <td>${user.surname}</td>
                            <td>${user.email}</td>
                            <td>${user.address}</td>
                            <td>${user.currency.abbr}</td>
                            <td>
                                <select class="form-control" id="userRoleSelect">
                                    <c:forEach items="${roles}" var="role">
                                        <c:set var="selected" value="" />
                                        <c:if test="${user.role.name eq role}">
                                            <c:set value="selected" var="selected" />
                                        </c:if>
                                        <option ${selected} value="${role}"><spring:message code="user.role.${role}" /></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="td-num"><button class="btn btn-danger delete-mark"  data-user-id="${user.id}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
</div><!-- /.row -->

<script type="text/javascript">
    $(document).ready(function() {
       var userId = '${userId}';
        if(userId) {
            $('*[data-user-id="' + userId + '"]').parents('tr').addClass('success');
        }

        $(document).on('change', '#userRoleSelect', function() {
            var newRole = $(this).val();
            var $tr = $(this).parents('tr');
            $.post('${home}/users/change-role',
                    {
                        role : newRole,
                        userId : $tr.find('.delete-mark').data('userId')
                    },
                    function(message) {
                if(message) {
                    displayFlashMessage(message);
                }
            })
        });

        $(document).on('click', '.delete-mark', function() {
           var id = $(this).data('userId');
            var $tr = $(this).parents('tr');
            if(id) {
                $.post('${home}/users/delete', {id : id}, function(message) {
                    if(message) {
                        displayFlashMessage(message);
                        if(message.key = FLASH_MESSAGE.success) {
                            $tr.remove();
                        }
                    }
                });
            }
        });
    });
</script>