<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${home}/"><spring:message code="overview"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li><a href="${home}/products"><i class="fa fa-dashboard">&nbsp;</i><spring:message code="product.plural" /></a></li>
            <li><a href="${home}/categories"><i class="fa fa-bar-chart-o">&nbsp;</i><spring:message code="category.plural"/></a></li>
            <li><a href="${home}/purchases"><i class="fa fa-table"></i>&nbsp;<spring:message code="purchase.plural"/></a></li>
            <li><a href="${home}/users"><i class="fa fa-edit"></i>&nbsp;<spring:message code="user.plural"/></a></li>
            <li><a href="${home}/settings"><i class="fa fa-wrench"></i>&nbsp;<spring:message code="settings"/></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right navbar-user">
            <c:choose>
                <c:when test="${totalNewPurchases > 0}">
                    <li class="dropdown messages-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i> <spring:message code="purchase.new" />
                            <span class="badge">${totalNewPurchases}</span> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <c:forEach items="${newPurchases}" var="purchase">
                                <li class="message-preview">
                                    <a href="${home}/purchases/${purchase.id}">
                                        <span class="name">$ ${purchase.total}</span>
                                        <span class="message">${purchase.customer.email}</span>
                                        <span class="time"><i class="fa fa-clock-o"></i>
                                            <fmt:formatDate value="${purchase.purchased}" pattern="dd-MM hh:mm" />
                                        </span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                            </c:forEach>
                            <li><a href="${home}/purchases"><spring:message code="purchase.view-all-new" /></a></li>
                        </ul>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="dropdown messages-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i> <spring:message code="purchase.new" />
                            <span class="badge">${totalNewPurchases}</span> <b class="caret"></b>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
            <%--<li class="dropdown user-dropdown">--%>
                <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>--%>
                <%--<ul class="dropdown-menu">--%>
                    <%--<li><a href="#"><i class="fa fa-user"></i> Profile</a></li>--%>
                    <%--<li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>--%>
                    <%--<li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>--%>
                    <%--<li class="divider"></li>--%>
                    <%--<li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>--%>
                <%--</ul>--%>
            <%--</li>--%>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>