<%@include file="/WEB-INF/views/utils/include.jsp" %>


<div class="row">
    <div class="col-lg-12">
        <h1><spring:message code="app.overview"/></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> <spring:message code="app.overview"/></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading">${totalProducts}</p>
                        <p class="announcement-text"><spring:message code="product.total" /></p>
                    </div>
                </div>
            </div>
            <a href="${home}/products">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            <spring:message code="go" />
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading">${fn:length(purchases)}</p>
                        <p class="announcement-text"><spring:message code="purchase.total" /></p>
                    </div>
                </div>
            </div>
            <a href="${home}/purchases">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            <spring:message code="go" />
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> <spring:message code="purchase.recent" /></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped tablesorter">
                        <thead>
                        <tr>
                            <th><spring:message code="total-price"/></th>
                            <th><spring:message code="customer" /></th>
                            <th><spring:message code="date" /></th>
                            <th><spring:message code="purchase.state" /></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${purchases}" var="purchase">
                            <tr>
                                <td><a href="${home}/purchases/${purchase.id}">$ ${purchase.total}</a></td>
                                <td><a href="${home}/users?id=${purchase.customer.id}">${purchase.customer.email}</a></td>
                                <td><fmt:formatDate value="${purchase.purchased}" pattern="dd-MM hh:mm" /></td>
                                <td><spring:message code="purchase.state.${fn:toLowerCase(purchase.state)}" /></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="text-right">
                    <a href="${home}/purchases"><spring:message code="purchase.view-all" /> <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.row -->