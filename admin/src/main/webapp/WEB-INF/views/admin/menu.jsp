<%@ include file="/WEB-INF/views/utils/include.jsp" %>

        <h2 class="page-header"><spring:message code="category.plural" /></h2>
<ul class="nav nav-stacked nav-pills">
    <c:forEach items="${categories}" var="category">
        <li><a href="${home}/catalog/${category.link}">${category.title}</a></li>
    </c:forEach>
</ul>