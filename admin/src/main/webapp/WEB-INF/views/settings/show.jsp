<%@include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <spring:message code="settings" />
        </h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li class="active"><spring:message code="settings" /></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2 class="panel-title"><spring:message code="currency.plural" /></h2>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th><spring:message code="currency.title" /></th>
                        <th><spring:message code="currency.abbr" /></th>
                        <th><spring:message code="currency.sign" /></th>
                        <th><spring:message code="currency.ratio-to-main" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <c:set var="mainStyle" value="success" />
                    <c:forEach items="${currencies}" var="item" varStatus="counter">
                        <c:if test="${counter.count != 1}">
                            <c:set var="mainStyle" value="" />
                        </c:if>
                        <tr class="${mainStyle}">
                            <td>${item.title}</td>
                            <td>${item.abbr}</td>
                            <td>${item.sign}</td>
                            <td>${item.rateToMain}</td>
                            <td class="td-num"><a href="${home}/settings/currencies/${item.id}" class="btn btn-default">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a></td>
                            <td class="td-num"><button class="btn btn-danger delete-mark"  data-curr-id="${item.id}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button></td>
                        </tr>
                    </c:forEach>
                </table>
                <a href="${home}/settings/currencies/add" class="btn btn-success pull-right">
                    <spring:message code="add" />
                </a>
            </div>
        </div>
    </div>
</div><!-- /.row -->

<script type="text/javascript">
    $(document).ready(function() {
        $('.delete-mark').click(function() {
            var $tr = $(this).parents('tr');
            var currId = $(this).data('currId');
            $.post('${home}/settings/currencies/delete', {id : currId}, function(data) {
                if(data) {
                    if(data.key == FLASH_MESSAGE.success) {
                        $tr.remove();
                    }
                    displayFlashMessage(data);
                }
            });
        });
    });
</script>