<%@ include file="/WEB-INF/views/utils/include.jsp"%>

<c:set var="update" value="${false}" />
<c:set var="action" value="${home}/settings/currencies/add" />
<spring:message code="currency.add" var="headers"/>
<spring:message code="add" var="button" />
<c:if test="${not empty currency.id}">
    <c:set var="update" value="${true}" />
    <c:set var="action" value="${home}/settings/currencies/update" />
    <c:set var="headers" value="${currency.title}" />
    <spring:message code="update" var="button" />
</c:if>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">${headers}</h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home" /></a></li>
            <li><a href="${home}/settings"><spring:message code="settings" /></a></li>
            <li class="active">${currency.title}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-7">
        <form:form
                commandName="currency"
                cssClass="form-horizontal"
                action="${action}"
                method="POST"
                enctype="multipart/form-data">
            <div class="form-group">
                <spring:message code="title" var="title"/>
                <form:label cssClass="control-label col-md-2" path="title">${title}</form:label>
                <div class="col-md-10">
                    <form:input path="title" cssClass="form-control" placeholder="${title}" value="${currency.title}"/>
                    <form:errors path="title" cssClass="form-error"/>
                </div>
            </div>
            <div class="form-group">
                <spring:message code="currency.abbr" var="abbr" />
                <form:label class="control-label col-md-2" path="abbr">${abbr}</form:label>
                <div class="col-md-10">
                    <form:input path="abbr" cssClass="form-control" placeholder="${abbr}" />
                    <form:errors path="abbr" cssClass="form-error" />
                </div>
            </div>
            <div class="form-group">
                <spring:message code="currency.sign" var="sign" />
                <form:label class="control-label col-md-2" path="sign">${sign}</form:label>
                <div class="col-md-10">
                    <form:input path="sign" cssClass="form-control" placeholder="${sign}" />
                    <form:errors path="sign" cssClass="form-error" />
                </div>
            </div>
            <div class="form-group">
                <spring:message code="currency.ratio-to-main" var="rateToMain" />
                <form:label class="control-label col-md-2" path="rateToMain">${rateToMain}</form:label>
                <div class="col-md-10">
                    <form:input path="rateToMain" cssClass="form-control" placeholder="${rateToMain}" />
                    <form:errors path="rateToMain" cssClass="form-error" />
                </div>
            </div>
            <c:if test="${update}">
                    <form:hidden path="id" />
            </c:if>
            <div class="form-group">
                <div class="col-md-2 col-md-offset-2">
                    <input type="submit" class="btn btn-success" value="${button}"/>
                </div>
            </div>
        </form:form>
    </div>
</div>
