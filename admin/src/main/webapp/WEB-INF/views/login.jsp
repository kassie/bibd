<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<script type="text/javascript">
    $(document).ready(function(){
        if(window.location.search.search(/state=error/) != -1) {
            $(".alert-error").removeClass("hidden");
        }
        $(".enterable").bind("keyup", function(e){
            var code = e.which;
            if(code == 13) {
                $("#loginForm").submit();
            }
        });
    });
</script>

<div class="span4 offset4">

    <form id="loginForm" class="form-horizontal well" method="POST" action="${home}/login-me">
        <h3 class="text-center">
            <span><spring:message code="login" /></span>
        </h3>

        <%--<div class="hidden alert alert-error text-center control-group">--%>
            <%--<strong><spring:message code="error" />:</strong>--%>
            <%--<c:choose>--%>
                <%--<c:when test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message == 'Bad credentials'}">--%>
                    <%--<spring:message code="app.login.bad-credentials" />--%>
                <%--</c:when>--%>
                <%--<c:when test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message == 'User is disabled'}">--%>
                    <%--<spring:message code="app.login.account-disabled" />--%>
                <%--</c:when>--%>
            <%--</c:choose>--%>
        <%--</div>--%>
        <div class="control-group">
            <label class="control-label"><spring:message code="email" /></label>
            <div class="controls">
                <input type="text" name="j_username" placeholder="<spring:message code='email' />" value="${userEmail}" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><spring:message code="password" /></label>
            <div class="controls">
                <input class="enterable" type="password" name="j_password" placeholder="<spring:message code='user.password' />" />
                <span class="help-block"><a href="${home}/login/forgot-password"><spring:message code="app.login.forgot-password" /></a></span>
            </div>
        </div>
        <div class="control-group low-control-group">
            <div class="controls">
                <input name="_spring_security_remember_me" type="checkbox" checked="checked" class="check">
                <span><spring:message code="app.remember-me" /></span>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <spring:message code="app.to-login" var="toLogin" />
                <input type="submit" class="submitFormBtn btn" value="${toLogin}" />&nbsp;&nbsp;<a href="${home}/registration"><spring:message code="app.to-register" /></a>
            </div>
        </div>
    </form>
</div>