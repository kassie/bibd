<%@ include file="/WEB-INF/views/utils/include.jsp" %>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">404 <small><spring:message code="error.404.title" /></small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="error-404"><spring:message code="error.404" /></p>
        <p class="lead"><spring:message code="error.404.desc" /></p>
    </div>
</div>