package by.bsuir.bibd.model.currency;

import by.bsuir.bibd.model.user.UserService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyDAO currencyDAO;

    @Autowired
    private UserService userService;

    @Override
    @Transactional(readOnly = true)
    public Currency findMain() {
        return currencyDAO.findByMain(true);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Currency> findAll() {
        return currencyDAO.findAll();
    }

    @Override
    @Transactional
    public Currency create(Currency currency) {
        return currencyDAO.saveAndFlush(currency);
    }

    @Override
    @Transactional(readOnly = true)
    public Currency findOne(Long id) {
        return currencyDAO.findOne(id);
    }

    @Override
    @Transactional
    public FlashMessage update(Currency currency) {
        Currency oldCurr = findOne(currency.getId());
        if(oldCurr == null) {
            return FlashMessage.error("currency.not-found");
        }
        currencyDAO.save(oldCurr.update(currency));
        return FlashMessage.success("currency.updated");
    }

    @Override
    @Transactional
    public FlashMessage delete(Long id) {
        if(findOne(id).isMain()) {
            return FlashMessage.error("currency.error.is-main");
        }
        userService.updateCurrencies(findMain());
        currencyDAO.delete(id);
        return FlashMessage.success("currency.deleted");
    }

    private void updateMainCurrency() {
        for(Currency c : findAll()) {
            c.setMain(false);
        }
        currencyDAO.save(findAll());
    }
}
