package by.bsuir.bibd.model.purchase;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PurchaseDAO extends JpaRepository<Purchase, Long> {

    public Page<Purchase> findByState(Pageable request, Purchase.State state);
    public List<Purchase> findByCustomerIdOrderByPurchasedDesc(Long id);
    public List<Purchase> findByStateOrderByPurchasedDesc(Purchase.State state);
}
