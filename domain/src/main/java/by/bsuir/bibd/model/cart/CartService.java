package by.bsuir.bibd.model.cart;

public interface CartService {

    public Cart create(Cart cart);
    public Cart update(Cart cart);
    public void clean(Cart cart);

//    public cart findByUserId(Long id);
}
