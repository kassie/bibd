package by.bsuir.bibd.model.currency;

import by.bsuir.bibd.util.FlashMessage;

import java.util.List;

public interface CurrencyService {

    public Currency findMain();
    public List<Currency> findAll();
    public Currency create(Currency currency);
    public Currency findOne(Long id);
    public FlashMessage update(Currency currency);
    public FlashMessage delete(Long id);
}
