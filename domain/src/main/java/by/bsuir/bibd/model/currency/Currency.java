package by.bsuir.bibd.model.currency;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Currency {

    private Long id;
    private String title;
    private String abbr;
    private String sign;
    private boolean main;
    private double rateToMain;


    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Size(max = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotBlank
    @Size(max = 3)
    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    @NotBlank
    @Size(max = 3)
    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    @NotNull
    @Min(0)
    public double getRateToMain() {
        return rateToMain;
    }

    public void setRateToMain(double rateToMain) {
        this.rateToMain = rateToMain;
    }

    @Transient
    public Currency update(Currency currency) {
        this.title = currency.getTitle();
        this.abbr = currency.getAbbr();
        this.sign = currency.getSign();
        this.rateToMain = currency.getRateToMain();
        return this;
    }
}
