package by.bsuir.bibd.model.purchase.PurchaseItem;

import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.purchase.Purchase;

import java.util.List;
import java.util.Set;

public interface PurchaseItemService {

    public PurchaseItem create(PurchaseItem purchaseItem);
    public List<PurchaseItem> createItemList(Set<Product> products, Purchase purchase);
    public List<PurchaseItem> findByPurchaseId(Long id);

    public void delete(Long id);
    public void delete(Purchase purchase);
}
