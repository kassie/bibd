package by.bsuir.bibd.model.currency;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyDAO extends JpaRepository<Currency, Long> {
    public Currency findByMain(boolean main);
}
