package by.bsuir.bibd.model.user;

import by.bsuir.bibd.model.cart.Cart;
import by.bsuir.bibd.model.cart.CartService;
import by.bsuir.bibd.model.currency.Currency;
import by.bsuir.bibd.model.currency.CurrencyService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Value("${user.items-on-page}")
    private Integer itemsOnPage;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private CartService cartService;

    @Autowired
    private CurrencyService currencyService;

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public User findOne(Long id) {
        return userDAO.findOne(id);
    }

    @Override
    @Transactional
    public User create(User user) {
//        user.setPassword(encodePass(user.getPassword()));
        user.setRole(User.Role.USER);
        user.setCurrency(currencyService.findMain());
        user.setCart(cartService.create(new Cart()));
        return userDAO.saveAndFlush(user);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        userDAO.delete(id);
    }

    @Override
    @Transactional
    public Page<User> findAll(Integer page) {
        Integer pageNumber = page == null ? 0 : page;
        PageRequest pageRequest =new PageRequest(pageNumber, itemsOnPage, new Sort(Sort.Direction.ASC, "role", "email"));
        return userDAO.findAll(pageRequest);
    }

    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    @Transactional(readOnly = true)
    public User findOne(HttpServletRequest req) {
        User user = null;
        if(req.getUserPrincipal() != null) {
            String email = req.getUserPrincipal().getName();
            User storedUser = findByEmail(email);
            if(storedUser != null) {
                user = storedUser;
            }
        }
        return user;
    }

    @Override
    @Transactional
    public FlashMessage update(User user) {
        User existingUser = findOne(user.getId());
        if(existingUser == null) {
            return FlashMessage.error("user.error.not-found");
        }
        userDAO.save(existingUser.update(user));
        return FlashMessage.success("user.updated");
    }

    @Override
    @Transactional
    public void updateCurrencies(Currency currency) {
        for(User u : findAll()) {
            u.setCurrency(currency);
            userDAO.save(u);
        }
    }

    @Override
    public boolean validRegistrationData(Model model, User user, String pass, String rePass) {
        if(findByEmail(user.getEmail()) != null) {
            model.addAttribute(FlashMessage.ERROR_KEY, FlashMessage.error("error.email.exists"));
            return false;
        }
        if("".equals(pass) || pass == null) {
            model.addAttribute(FlashMessage.ERROR_KEY, FlashMessage.error("error.pass.invalid"));
            return false;
        }
        if(!pass.equals(rePass)) {
            model.addAttribute(FlashMessage.ERROR_KEY, FlashMessage.error("error.pass.not-equal"));
            return false;
        }
        return true;
    }

    private String encodePass(String pass) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(pass);
    }
}
