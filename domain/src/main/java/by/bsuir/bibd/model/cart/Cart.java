package by.bsuir.bibd.model.cart;

import by.bsuir.bibd.model.product.Product;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
public class Cart {

    private Long id;
//    private User user;
    private Integer count = 0;
    private Set<Product> products;

    private BigDecimal total;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    @OneToOne
//    @JoinColumn(name = "user_id")
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }

    @ManyToMany(fetch = FetchType.EAGER)
    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Transient
    public BigDecimal getTotal() {
        BigDecimal total = new BigDecimal(0);
        for(Product p : this.products) {
            total = total.add(p.getPrice());
        }
        return total;
    }

}
