package by.bsuir.bibd.model.cart;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CartDAO extends JpaRepository<Cart, Long> {

//    public cart findByUserId(Long id);
}
