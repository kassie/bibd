package by.bsuir.bibd.model.category;

import org.springframework.data.domain.Page;

import java.util.List;

public interface CategoryService {

    public List<Category> findAll();
    public Page<Category> findAll(Integer page, Integer itemsOnPage);
    public Category findOne(Long id);
    public Category findByLink(String link);
    public Category findByTitle(String title);
    public List<Category> findByTitleContaining(String title);
    public Category create(Category category);
    public Category update(Category category);
    public void delete(Long id);
}
