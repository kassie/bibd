package by.bsuir.bibd.model.product;

import by.bsuir.bibd.model.category.Category;
import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.model.user.UserService;
import by.bsuir.bibd.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private FileService fileService;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public Product create(Product product) {
        product.setCreated(new Date());
        return productDAO.saveAndFlush(product);
    }

    @Override
    @Transactional
    public Product save(Product product) {
        return productDAO.save(product);
    }

    @Override
    @Transactional(readOnly = true)
    public Product findOne(Long id) {
        return productDAO.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return productDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Product> findByCategory(Long id, Integer page, Integer itemsOnPage) {
        Integer pageNumber = page == null ? 0 : page;
        PageRequest pageRequest = new PageRequest(pageNumber, itemsOnPage, new Sort(Sort.Direction.DESC, "created"));
        return productDAO.findByCategoryId(id, pageRequest);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Product> findAll(Integer page, Integer itemsOnPage) {
        Integer pageNumber = page == null ? 0 : page;
        PageRequest pageRequest = new PageRequest(pageNumber, itemsOnPage, new Sort(Sort.Direction.DESC, "created"));
        return productDAO.findAll(pageRequest);
    }

    @Override
    @Transactional(readOnly = false)
    public List<Product> findByCategory(Long id) {
        return productDAO.findByCategoryId(id);
    }

    @Override
    @Transactional
    public Product update(Product product, MultipartFile image, Category category) {
        Product oldProd = findOne(product.getId());
        oldProd.update(product);
        oldProd.setCategory(category);
        if(image.getSize() != 0) {
            oldProd.setPhoto(fileService.uploadFile(image));
        }
        return save(oldProd);
    }

    @Override
    @Transactional
    public int updateProducts(List<Product> products) {
        int counter = 0;
        for(Product product : products) {
            if(!isValidProduct(product)) continue;
            counter++;
            Product oldProd = findByArticul(product.getArticul());
            if(oldProd == null) {
                create(product);
            } else {
                //TODO import active flag
                product.setActive(true);
                save(oldProd.update(product));
            }
        }
        return counter;
    }

    private boolean isValidProduct(Product product) {
        if(product == null) return false;
        if(product.getTitle() == null) return false;
        if(product.getArticul() == null) return false;
        if(product.getPrice() == null || product.getPrice().equals(BigDecimal.ZERO)) return false;
        if(product.getCategory() == null) return false;
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public Product findByArticul(String articul) {
        return productDAO.findByArticul(articul);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        productDAO.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Product setInCart(HttpServletRequest req, Product product) {
        User customer = userService.findOne(req);
        if(customer != null) {
            product.setInCart(customer.getCart().getProducts().contains(product));
        }
        return product;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> getProductsForExport() {
        return productDAO.findAll(new Sort(Sort.Direction.ASC, "category_id"));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findByTitleContaining(String title) {
        return productDAO.findByTitleContainingAndActive(title, true);
    }
}

