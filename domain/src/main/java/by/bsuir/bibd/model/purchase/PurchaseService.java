package by.bsuir.bibd.model.purchase;

import by.bsuir.bibd.model.cart.Cart;
import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PurchaseService {

    public Purchase create(Purchase purchase);
    public Purchase create(Cart cart, User customer);
    public Purchase findOne(Long id);
    public List<Purchase> findAll();
    public Page<Purchase> findByState(Purchase.State state, Integer page);
    public List<Purchase> findByState(Purchase.State state);
    public List<Purchase> findByCustomerId(Long id);
    public FlashMessage update(Purchase purchase);

    public FlashMessage delete(Long id);
}
