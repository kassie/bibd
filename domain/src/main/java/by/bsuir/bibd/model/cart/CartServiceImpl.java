package by.bsuir.bibd.model.cart;

import by.bsuir.bibd.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartDAO cartDAO;

    @Override
    @Transactional
    public Cart create(Cart cart) {
        return cartDAO.saveAndFlush(cart);
    }

    @Override
    @Transactional
    public Cart update(Cart cart) {
        cart.setCount(cart.getProducts().size());
        return cartDAO.save(cart);
    }

    @Override
    @Transactional
    public void clean(Cart cart) {
        cart.setProducts(new HashSet<Product>());
        cart.setCount(0);
        cartDAO.save(cart);
    }

    //    @Override
//    @Transactional(readOnly = true)
//    public cart findByUserId(Long id) {
//        return cartDAO.findByUserId(id);
//    }
}
