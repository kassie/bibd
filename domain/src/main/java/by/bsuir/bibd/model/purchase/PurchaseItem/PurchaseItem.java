package by.bsuir.bibd.model.purchase.PurchaseItem;

import by.bsuir.bibd.model.purchase.Purchase;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class PurchaseItem {

    private Long id;
    private String title;
    private BigDecimal price;
    private Purchase purchase;
    private Long productId;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @ManyToOne
    @JoinColumn(name = "purchase_id")
    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
}
