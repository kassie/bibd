package by.bsuir.bibd.model.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductDAO extends JpaRepository<Product, Long> {

    public Page<Product> findByCategoryId(Long id, Pageable pageReq);
    public List<Product> findByCategoryId(Long id);
    public Product findByArticul(String articul);

    public List<Product> findByTitleContainingAndActive(String title, boolean active);
}
