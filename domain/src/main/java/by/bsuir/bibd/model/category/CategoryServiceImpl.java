package by.bsuir.bibd.model.category;

import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Transient;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private ProductService productService;

    @Override
    @Transactional(readOnly = true)
    public Category findOne(Long id) {
        return categoryDAO.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Category> findAll() {
        return categoryDAO.findAll();
    }

    @Override
    @Transactional
    public Category create(Category category) {
        category.setLink(checkLink(category));
        return categoryDAO.saveAndFlush(category);
    }

    @Override
    @Transactional(readOnly = true)
    public Category findByTitle(String title) {
        return categoryDAO.findByTitle(title);
    }

    @Override
    @Transactional
    public Category update(Category category) {
        Category oldCat = findOne(category.getId());
        oldCat.setLink(checkLink(category));
        oldCat.update(category);
        return categoryDAO.save(oldCat);
    }

    @Override
    @Transactional
    public void delete (Long id) {
        for(Product p : productService.findByCategory(id)) {
            productService.delete(p.getId());
        }
        categoryDAO.delete(id);
    }

    @Override
    @Transactional
    public Page<Category> findAll(Integer page, Integer itemsOnPage) {
        Integer pageNumber = page == null ? 0 : page;
        PageRequest pageReq = new PageRequest(pageNumber, itemsOnPage, new Sort(Sort.Direction.ASC, "title"));
        return categoryDAO.findAll(pageReq);
    }

    @Override
    @Transactional
    public Category findByLink(String link) {
        return categoryDAO.findByLink(link);
    }

    @Transient
    private String checkLink(Category category) {
        String link = category.getLink();
        if(link == null || link.trim().equals("")) {
            link = StringUtil.toTranslit(category.getTitle());
        } else {
            link = StringUtil.toTranslit(category.getLink());
        }
        if(category.getId() == null) {
            while(categoryDAO.findByLink(link) != null) {
                link = nextLink(link);
            }
        } else {
            while(categoryDAO.findByLinkAndIdNot(link, category.getId()) != null) {
                link = nextLink(link);
            }
        }
        return link;
    }

    @Transient
    private String nextLink(String link) {
        if(link.indexOf("_") > 0) {
            String strIden = link.substring(link.lastIndexOf("_")  + 1);
            try {
                Integer intIden = Integer.parseInt(strIden);
                return link.substring(0, link.lastIndexOf("_") + 1) + ++intIden;
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return link + "_1";
    }

    @Override
    @Transactional(readOnly = true)
    public List<Category> findByTitleContaining(String title) {
        return categoryDAO.findByTitleContaining(title);
    }
}
