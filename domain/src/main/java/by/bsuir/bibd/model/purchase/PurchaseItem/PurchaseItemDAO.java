package by.bsuir.bibd.model.purchase.PurchaseItem;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PurchaseItemDAO extends JpaRepository<PurchaseItem, Long> {

    public List<PurchaseItem> findByPurchaseId(Long id);
}
