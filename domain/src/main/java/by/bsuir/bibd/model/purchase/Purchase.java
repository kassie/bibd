package by.bsuir.bibd.model.purchase;

import by.bsuir.bibd.model.currency.Currency;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItem;
import by.bsuir.bibd.model.user.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Purchase {

    private Long id;
    private Date purchased;
    private BigDecimal total;
    private State state;
    private User customer;

    private Currency currency;
    private List<PurchaseItem> items;

    public enum State {
        NEW, DELIVERING, DONE;

        public static List<String> getStateList() {
            State[] list = values();
            List<String> result = new ArrayList<>();
            for(State s : list) {
                result.add(s.toString());
            }
            return result;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getPurchased() {
        return purchased;
    }

    public void setPurchased(Date purchased) {
        this.purchased = purchased;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Enumerated(EnumType.STRING)
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    @Transient
    public List<PurchaseItem> getItems() {
        return items;
    }

    public void setItems(List<PurchaseItem> items) {
        this.items = items;
    }

    @Transient
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Purchase update(Purchase purchase) {
        this.state = purchase.state;
        return this;
    }

}
