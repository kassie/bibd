package by.bsuir.bibd.model.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDAO  extends JpaRepository<User, Long> {

    public User findByEmail(String email);
}
