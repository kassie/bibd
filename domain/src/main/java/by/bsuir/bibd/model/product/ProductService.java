package by.bsuir.bibd.model.product;

import by.bsuir.bibd.model.category.Category;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ProductService {

    public Product create(Product product);
    public Product save(Product product);
    public Product findOne(Long id);
    public Page<Product> findAll(Integer page, Integer itemsOnPage);
    public Page<Product> findByCategory(Long id, Integer page, Integer  itemsOnPage);
    public List<Product> findByCategory(Long id);
    public List<Product> findAll();
    public Product update(Product product, MultipartFile image, Category category);
    public void delete(Long id);
    public List<Product> getProductsForExport();
    public int updateProducts(List<Product> products);
    public Product findByArticul(String articul);
    public List<Product> findByTitleContaining(String title);

    public Product setInCart(HttpServletRequest req, Product product);
}
