package by.bsuir.bibd.model.purchase;

import by.bsuir.bibd.model.cart.Cart;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItemService;
import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class PurchaseServiceImpl implements PurchaseService {

//    @Value("${purchase.admin.items-on-page}")
    private static final Integer ITEMS_ON_PAGE = 10;

    @Value("${settings.int}")
    private Integer intval;
    @Value("${settings.str}")
    private String strval;

    @Autowired
    private PurchaseDAO purchaseDAO;

    @Autowired
    private PurchaseItemService purchaseItemService;

    @Override
    @Transactional
    public Purchase create(Purchase purchase) {
        purchase.setPurchased(new Date());
        return purchaseDAO.saveAndFlush(purchase);
    }

    @Override
    public Purchase create(Cart cart, User customer) {
        Purchase purchase = new Purchase();
        purchase.setTotal(cart.getTotal());
        purchase.setState(Purchase.State.NEW);
        purchase.setCustomer(customer);
        purchase = create(purchase);
        purchaseItemService.createItemList(cart.getProducts(), purchase);
        return purchase;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Purchase> findByState(Purchase.State state, Integer page) {
        Integer pageNumber = page == null ? 0 : page;
        PageRequest request = new PageRequest(pageNumber, ITEMS_ON_PAGE, new Sort(Sort.Direction.DESC, "purchased"));
        return purchaseDAO.findByState(request, state);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Purchase> findByCustomerId(Long id) {
        return purchaseDAO.findByCustomerIdOrderByPurchasedDesc(id);
    }

    @Override
    @Transactional
    public Purchase findOne(Long id) {
        return purchaseDAO.findOne(id);
    }

    @Override
    @Transactional
    public FlashMessage update(Purchase purchase) {
        Purchase oldPurchase = findOne(purchase.getId());
        if(oldPurchase == null) {
            return FlashMessage.error("purchase.error.update");
        }
        oldPurchase.update(purchase);
        purchaseDAO.save(oldPurchase);
        return FlashMessage.success("purchase.success.updated");
    }

    @Override
    @Transactional(readOnly = true)
    public List<Purchase> findAll() {
        return purchaseDAO.findAll();
    }

    @Override
    @Transactional
    public FlashMessage delete(Long id) {
        Purchase purchase = purchaseDAO.findOne(id);
        if(purchase == null) {
            return FlashMessage.error("purchase.not-found");
        }
        purchaseItemService.delete(purchase);
        purchaseDAO.delete(id);
        return FlashMessage.success("purchase.deleted");
    }

    @Override
    @Transactional(readOnly = true)
    public List<Purchase> findByState(Purchase.State state) {
        return purchaseDAO.findByStateOrderByPurchasedDesc(state);
    }

}
