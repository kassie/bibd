package by.bsuir.bibd.model.user;

import by.bsuir.bibd.model.cart.Cart;
import by.bsuir.bibd.model.currency.Currency;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String address;
    private Role role;
    private int phone;
    private String password;
    private Cart cart;
    private Currency currency;

    public enum Role {
        USER, ADMIN;

        public static List<String> getList() {
            List<String> result = new ArrayList<>();
            for(Role r : values()) {
                result.add(r.toString());
            }
            return result;
        }

        public String getName() {
            return this.name();
        }
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Size(max =250)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(max = 250)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @NotEmpty
    @Size(max = 250)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Size(max = 1000)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Enumerated(EnumType.STRING)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToOne
    @JoinColumn(name = "currency_id")
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @OneToOne
    @JoinColumn(name = "cart_id")
    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) {return true; }
        if(obj == null || !(obj instanceof User)) {return false;}
        User user = (User)obj;
        return user.getId().equals(this.id) && user.getEmail().equals(this.email);
    }

    @Override
    public int hashCode() {
        int result = 31;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + email.hashCode();
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (cart != null ? cart.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.email;
    }

    public User update(User user) {
        this.name = user.getName();
        this.surname = user.getSurname();
        this.address = user.getAddress();
        this.phone = user.getPhone();
        this.currency = user.getCurrency();
        this.role = user.getRole();
        return this;
    }
}
