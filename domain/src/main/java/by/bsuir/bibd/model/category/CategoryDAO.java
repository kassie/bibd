package by.bsuir.bibd.model.category;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryDAO extends JpaRepository<Category, Long> {

    public Category findByLink(String link);
    public Category findByLinkAndIdNot(String link, Long id);
    public Category findByTitle(String title);
    public List<Category> findByTitleContaining(String title);
}
