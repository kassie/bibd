package by.bsuir.bibd.model.purchase.PurchaseItem;

import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.purchase.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class PurchaseItemServiceImpl implements PurchaseItemService {

    @Autowired
    private PurchaseItemDAO purchaseItemDAO;

    @Override
    @Transactional
    public PurchaseItem create(PurchaseItem purchaseItem) {
        return purchaseItemDAO.saveAndFlush(purchaseItem);
    }

    @Override
    public List<PurchaseItem> createItemList(Set<Product> products, Purchase purchase) {
        List<PurchaseItem> items = new ArrayList<>();
        for(Product p : products) {
            PurchaseItem item = new PurchaseItem();
            item.setPrice(p.getPrice());
            item.setProductId(p.getId());
            item.setTitle(p.getTitle());
            item.setPurchase(purchase);
            items.add(create(item));
        }
        return items;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PurchaseItem> findByPurchaseId(Long id) {
        return purchaseItemDAO.findByPurchaseId(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        purchaseItemDAO.delete(id);
    }

    @Override
    @Transactional
    public void delete(Purchase purchase) {
        for(PurchaseItem pi : findByPurchaseId(purchase.getId())) {
            delete(pi.getId());
        }
    }
}
