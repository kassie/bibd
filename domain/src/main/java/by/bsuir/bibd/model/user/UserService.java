package by.bsuir.bibd.model.user;

import by.bsuir.bibd.model.currency.Currency;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {

    public List<User> findAll();
    public User findOne(Long id);
    public Page<User> findAll(Integer page);
    public User findOne(HttpServletRequest req);
    public User findByEmail(String email);
    public User create(User user);
    public void delete(Long id);
    public FlashMessage update(User user);
    public void updateCurrencies(Currency currency);

    public boolean validRegistrationData(Model model, User user, String pass, String rePass);
}
