package by.bsuir.bibd.util;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.Set;

public class FileUtil {

    //TODO correct properties
	private static final Logger logger = Logger.getLogger(FileUtil.class);
	private static final Set<String> allowedImageFormats = new HashSet<>();
	private static final int MAX_IMAGE_SIZE = 200;
	
	/* Validate file */

	public static FlashMessage validateImageFile(MultipartFile file) {
		if(file == null) {
			logger.error("file is null");
			return FlashMessage.error("file.is.null");
		}
		if(file.getSize() > MAX_IMAGE_SIZE) {
			String message = "Image " + file.getOriginalFilename() + " is too big (" + new Double(file.getSize() / 1000).toString() + " Kb).";
			logger.error(message);
			return FlashMessage.error("wrong.image.file.size");
		}
		
		if(!isFileIsImage(file.getOriginalFilename())) {
			String message = "Image have unacceptable extension. (original filename: \"" + file.getOriginalFilename() + "\"";
			logger.error(message);
			return FlashMessage.error("wrong.image.file.dimension");
		}
		return FlashMessage.success("image.update.success");
	}
	
	/* Utilities */
	
	private static boolean isFileIsImage(String fileName) {
		String extension = getFileExtension(fileName);
		if(allowedImageFormats.contains(extension)) {
			return true;
		}
		return false;
	}
	
	private static String getFileExtension(String fileName) {
		int dotIndex = fileName.lastIndexOf(".");
		if(dotIndex == -1) {
			return "";
		}
		return fileName.substring(dotIndex, fileName.length()).toLowerCase();
	}
	
}
