package by.bsuir.bibd.util;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	private static final Logger logger = Logger.getLogger(StringUtil.class);
	private static final String WHITESPACE_STRING_REGEXP = "^\\s*$";
	private static final String EMAIL_REGEXP = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final List<String> ENGLISH = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
//    private static final int CODE_LENGTH = SettingsRetriever.getInteger("code-length");

	private static final String EMPTY_TEXTAREA = "<br>";

    private static final String STRIP_TAGS_PATTERN = "<[^<>]+>";
	
	private static Pattern pattern;
	private static Matcher matcher;

    private static final Map<String, String> russian = new HashMap<>();
    static {
        russian.put(" ", "-");
        russian.put("а", "a");
        russian.put("б", "b");
        russian.put("в", "v");
        russian.put("г", "g");
        russian.put("д", "d");
        russian.put("е", "e");
        russian.put("ё", "e");
        russian.put("ж", "zh");
        russian.put("з", "z");
        russian.put("и", "i");
        russian.put("й", "i");
        russian.put("к", "k");
        russian.put("л", "l");
        russian.put("м", "m");
        russian.put("н", "n");
        russian.put("о", "o");
        russian.put("п", "p");
        russian.put("р", "r");
        russian.put("с", "s");
        russian.put("т", "t");
        russian.put("у", "u");
        russian.put("ф", "f");
        russian.put("х", "h");
        russian.put("ц", "c");
        russian.put("ч", "ch");
        russian.put("ш", "sh");
        russian.put("щ", "sh");
        russian.put("ъ", "'");
        russian.put("ы", "y");
        russian.put("ъ", "'");
        russian.put("э", "e");
        russian.put("ю", "u");
        russian.put("я", "ya");
    }
	
    public static String toTranslit(String text) {

        StringBuilder sb = new StringBuilder(text.length());
        for (int i = 0; i<text.length(); i++) {
            String letter = text.substring(i, i+1).toLowerCase();
            if (russian.containsKey(letter)) {
                sb.append(russian.get(letter));
            }
            else if(isEnglish(letter)) {
                sb.append(letter);
            }
        }
        return sb.toString();
    }
    
    private static boolean isEnglish(String letter) {
        return ENGLISH.contains(letter);
    }
    
	public static List<Long> delimitedStringToListOfLong(String toSplit, String delimiter){
		if(isEmpty(toSplit) || isEmpty(delimiter)) {
			return Collections.emptyList();
		}
		
		ArrayList<Long> resultList = new ArrayList<Long>();
		
		String[] elements = toSplit.split(delimiter);
		for(String element: elements) {
			try {
				resultList.add(Long.valueOf(element.trim()));
			}catch(NumberFormatException e) {
				logger.warn("Wrong long value [" + element + "]. This element will be skipped.");
			}
		}
		return resultList;
	}
	
    public static boolean isEmpty(String toTest) {
    	return toTest == null || toTest.trim().length() == 0 || toTest.matches(WHITESPACE_STRING_REGEXP);
    }
    
    public static boolean validateEmail(String address) {
    	matcher = pattern.matcher(address);
    	return !matcher.matches();
    }
    
//	public static String generateCode() {
//		StringBuilder code = new StringBuilder(CODE_LENGTH);
//		for(int i=0; i < CODE_LENGTH; i++) {
//			int num = (new Double(Math.random()*10)).intValue();
//			if(num % 2 == 0) {
//				code.append((new Double(Math.random()*10)).intValue());
//			} else {
//				code.append(generateRandomLetter());
//			}
//		}
//		return code.toString();
//	}
	
	private static String generateRandomLetter() {
		int character=(int)(Math.random()*52);
		String s=ALPHABET.substring(character, character+1);
		return s;
	}
	
	public static String getI18nMessage(String defaultValue, MessageSource messageSource, Locale locale, String messageCode) {
		String subject = defaultValue;
        try {
        	subject = messageSource.getMessage(messageCode, null, locale);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return subject;
	}
	
	public static String trim(String value) {
		if(EMPTY_TEXTAREA.equals(value)) {
			value = null;
		}
		return value;
	}

    public static String stripTags(String s) {
        return s.replaceAll(STRIP_TAGS_PATTERN, "");
    }

    public static String bold(String s, String substring) {
        return s.replaceAll("(?i)" + substring, "<b>" + substring + "</b>");
    }

}
