//package by.bsuir.bibd.util;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//import org.joda.time.DateTime;
//import org.joda.time.DateTimeZone;
//
//public class DateUtil {
//	private static final Logger logger = Logger.getLogger(DateUtil.class);
//
//	public static Date getByNowDate() {
//		DateTime now = new DateTime(System.currentTimeMillis(), DateTimeZone.forID("Europe/Minsk"));
//		Date neededDate = null;
//		try {
//			neededDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(now.toString("yyyy-MM-dd HH:mm:ss"));
//		} catch (ParseException e) {
//			logger.error("Error due parsing date due getting today date in Belarus.", e);
//		}
//		return neededDate;
//	}
//
//	public static Date getByAfterWeekDate(Date date) {
//		Date neededDate = null;
//		if(date != null) {
//			DateTime jodaDate = new DateTime(date.getTime(), DateTimeZone.forID("Europe/Minsk"));
//			jodaDate = jodaDate.plusWeeks(1);
//
//			try {
//				neededDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jodaDate.toString("yyyy-MM-dd HH:mm:ss"));
//			} catch (ParseException e) {
//				logger.error("Error due parsing date due getting today date in Belarus.", e);
//			}
//		}
//		return neededDate;
//	}
//
//	public static boolean isAfterWeeks(Date date, int weeksCount) {
//		boolean result = false;
//		if(date != null) {
//			DateTime afterWeeks = new DateTime(System.currentTimeMillis(), DateTimeZone.forID("Europe/Minsk"));
//			afterWeeks = afterWeeks.plusWeeks(weeksCount);
//			DateTime sessionStart = new DateTime(date.getTime(), DateTimeZone.forID("Europe/Minsk"));
//			if(afterWeeks.getYear()	== sessionStart.getYear() &&
//				afterWeeks.getMonthOfYear() == sessionStart.getMonthOfYear() &&
//				afterWeeks.getDayOfMonth() == sessionStart.getDayOfMonth()) {
//				result = true;
//			}
//		}
//		return result;
//	}
//
//	public static boolean isAfterMonth(Date date) {
//		boolean result = false;
//		if(date != null) {
//			DateTime afterMonth = new DateTime(System.currentTimeMillis(), DateTimeZone.forID("Europe/Minsk"));
//			afterMonth = afterMonth.plusMonths(1);
//			DateTime sessionStart = new DateTime(date.getTime(), DateTimeZone.forID("Europe/Minsk"));
//			if(afterMonth.getYear()	== sessionStart.getYear() &&
//				afterMonth.getMonthOfYear() == sessionStart.getMonthOfYear() &&
//				afterMonth.getDayOfMonth() == sessionStart.getDayOfMonth()) {
//				result = true;
//			}
//		}
//		return result;
//	}
//
//}
