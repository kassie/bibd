package by.bsuir.bibd.util;

public class FlashMessage {

    public static final String SUCCESS_KEY ="flashSuccessMessage";
    public static final String ERROR_KEY ="flashErrorMessage";

    private String code;
    private String message;
    private String key;


    private FlashMessage(String s, String key) {
        this.code = s;
        this.key = key;
    }

    public static FlashMessage success(String code) {
        return new FlashMessage(code, SUCCESS_KEY);
    }

    public static FlashMessage error(String code) {
        return new FlashMessage(code, ERROR_KEY);
    }

    public String getCode() {
        return code;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
