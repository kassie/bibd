package by.bsuir.bibd.service.export;

import by.bsuir.bibd.model.currency.Currency;
import by.bsuir.bibd.model.product.Product;

import java.util.List;

public class ProductExportService extends ExportService {

    private List<Product> products;
    private Currency currency;

    public ProductExportService(List<Product> products, Currency currency) {
        this.products = products;
        this.currency = currency;
    }

    @Override
    protected void toCSV() {
        insertStats();
        nextLine();
        insertHeaders();
        insertItems();
    }

    private void insertStats() {
        insertValue("Total items:");
        insertValue(products.size());
        insertValue("Currency");
        insertValue(currency.getAbbr());
        nextLine();
    }

    private void insertHeaders() {
        insertValue("Articul");
        insertValue("Title");
        insertValue("Price");
        insertValue("Photo");
        insertValue("Category Code");
        insertValue("Category Title");
        insertValue("Description");
        nextLine();
    }

    private void insertItems() {
        for(Product p : products) {
            insertValue(p.getArticul());
            insertValue(p.getTitle());
            insertValue(p.getPrice());
            insertValue(p.getPhoto());
            insertValue(p.getCategory().getId());
            insertValue(p.getCategory().getTitle());
            insertValue(p.getDescription());
            nextLine();
        }
    }
}
