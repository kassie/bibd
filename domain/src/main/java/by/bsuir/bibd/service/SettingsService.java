package by.bsuir.bibd.service;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.Set;

@Service
public class SettingsService {

	private ResourceBundle bundle = ResourceBundle.getBundle("settings");

	public Character getChar(String key) {
		return bundle.getString(key).charAt(0);
	}

	public String getString(String key) {
		return bundle.getString(key);
	}

	public Integer getInteger(String key) {
		return Integer.parseInt(bundle.getString(key));
	}

	public Set<String> getArray(String key) {
		Set<String> resultSet = new HashSet<String>();
		String source = bundle.getString(key);
		Scanner scanner = new Scanner(source);
		scanner.useDelimiter(",");
		while(scanner.hasNext()) {
			resultSet.add(scanner.next().toLowerCase());
		}
		scanner.close();
		return resultSet;
	}

}
