package by.bsuir.bibd.service.export;

import by.bsuir.bibd.model.currency.Currency;
import by.bsuir.bibd.model.purchase.Purchase;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItem;

import java.util.Date;
import java.util.List;

public class PurchaseExportService extends ExportService {

    private Purchase purchase;

    public PurchaseExportService(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    protected void toCSV() {
        insertDate(purchase.getPurchased());
        insertTotalCost(purchase.getTotal());
        insertCurrency(purchase.getCurrency());
        nextLine();
        insertItems(purchase.getItems());
    }

    private void insertItems(List<PurchaseItem> items) {
        insertValue("Products");
        insertValue("Price");
        nextLine();
        for(PurchaseItem p : items) {
            insertValue(p.getTitle());
            insertValue(p.getPrice());
            nextLine();
        }
        nextLine();
        insertTotalNumber(items);
    }

    private void insertCurrency(Currency currency) {
        insertValue("Currency");
        insertValue(currency.getAbbr());
        nextLine();
    }

    private void insertTotalCost(Object val) {
        insertValue("Total cost");
        insertValue(val);
        nextLine();
    }

    private void insertDate(Date date) {
        insertValue("Date");
        insertValue(date);
        nextLine();
    }

    private void insertTotalNumber(List<PurchaseItem> items) {
        insertValue("Total number: " + items.size());
    }
}
