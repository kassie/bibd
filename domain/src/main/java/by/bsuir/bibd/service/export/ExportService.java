package by.bsuir.bibd.service.export;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public abstract class ExportService {

    public static final char CSV_SEPARATOR = '\t';
    public static final int PRODUCTS_HEADER_LINES = 3;

    private List<List<String>> result;
    private List<String> lines;

    public final void export(OutputStream os) {
        try(CSVWriter writer = new CSVWriter(new OutputStreamWriter(os), CSV_SEPARATOR)) {
            result = new ArrayList<>();
            lines = new ArrayList<>();
            toCSV();
            String[] arr = {};
            for(List<String> line : result) {
                writer.writeNext(line.toArray(arr));
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract void toCSV();

    protected void nextLine() {
        result.add(lines);
        lines = new ArrayList<>();
    }

    protected <T> void insertValue(T val) {
        lines.add(val.toString());
    }
}
