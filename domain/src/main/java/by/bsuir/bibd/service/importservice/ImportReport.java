package by.bsuir.bibd.service.importservice;

import java.util.ArrayList;
import java.util.List;

public class ImportReport {

    private int totalItems;
    private int importedItems;
    private List<String> errors = new ArrayList<>();

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void addError(String error, int line, String prodInfo) {
        errors.add(error + " at line " + line + "; Product " + prodInfo);
    }

    public void addItem() {
        totalItems++;
    }

    public int getImportedItems() {
        return importedItems;
    }

    public void setImportedItems(int importedItems) {
        this.importedItems = importedItems;
    }
}
