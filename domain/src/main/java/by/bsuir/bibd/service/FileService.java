package by.bsuir.bibd.service;


import by.bsuir.bibd.util.ApplicationException;
import com.dropbox.core.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Locale;

@Service
public class FileService {
    private static final Logger logger = Logger.getLogger(FileService.class);
    private static final String PUBLIC_FOLDER = "/public/";

    private Object monitor = new Object();
    private DbxClient connection;

    @Value("${dropbox.public-url-prefix}")
    private String URL_PREFIX;

    @Value("${dropbox.project-dir}")
    private String PROJECT_DIR;

    @Value("${dropbox.access-token}")
    public void setConnection(String accessToken) {
        String userLocale = Locale.getDefault().toString();
        DbxRequestConfig requestConfig = new DbxRequestConfig("bibd.eshop", userLocale);
        this.connection = new DbxClient(requestConfig, accessToken);
    }

    public String uploadFile(MultipartFile file) {
        String url = null;
        File tmpFile = new File(file.getOriginalFilename());
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(tmpFile);
            fos.write(file.getBytes());
            fos.flush();
            fos.close();
            String fileName = PROJECT_DIR + System.currentTimeMillis() +  file.getOriginalFilename().hashCode() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
            uploadFileToDropbox(fileName, tmpFile);
            url = URL_PREFIX + fileName;
            tmpFile.delete();
        } catch (Exception e) {
            logger.error(e);
        }
        return url;
    }

    private String uploadFileToDropbox(String fileName, File uploadFile) {
        String resultFileName = null;
        FileInputStream uploadFIS = null;
        if(uploadFile == null) {
            throw new ApplicationException("File for upload must not be null");
        }
        try {
            uploadFIS = new FileInputStream(uploadFile);
            connection.uploadFile(PUBLIC_FOLDER + fileName, DbxWriteMode.add(), uploadFile.length(), uploadFIS);
            resultFileName = URL_PREFIX + fileName;
        } catch (Exception e) {
            logger.error(e);
            try {
                if(uploadFIS != null) {
                    uploadFIS.close();
                }
            } catch (IOException e1) {
                logger.error(e1);
            }
            throw new ApplicationException(e.getMessage());
        }
        return resultFileName;
    }

    public InputStream getInputStream(String path) {
        InputStream is = null;

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(path);
        HttpResponse response;
        try {
            response = client.execute(request);
            is = response.getEntity().getContent();
        } catch (IOException e) {
            logger.error(e);
        } catch (IllegalStateException e) {
            logger.error(e);
        }

        return is;
    }

    public void delete(String filePath) {
        if(filePath == null) {
            return;
        }
        try {
            String dbFilePath = filePath.substring(filePath.lastIndexOf("/") + 1);
            connection.delete(PUBLIC_FOLDER + dbFilePath);
        } catch (DbxException e) {
            logger.error(e);
        }
    }

}
