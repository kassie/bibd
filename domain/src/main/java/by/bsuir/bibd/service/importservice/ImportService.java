package by.bsuir.bibd.service.importservice;

import au.com.bytecode.opencsv.CSVReader;
import by.bsuir.bibd.model.category.Category;
import by.bsuir.bibd.model.category.CategoryService;
import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.service.export.ExportService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ImportService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    private static final Logger logger = Logger.getLogger(ImportService.class);

    private static final int ARTICUL = 0;
    private static final int TITLE = 1;
    private static final int PRICE = 2;
    private static final int PHOTO = 3;
    private static final int CATEGORY_ID = 4;
    private static final int CATEGORY_TITLE = 5;
    private static final int DESCRIPTION = 6;

    private ImportReport importReport;
    private int lineNumber;
    private File tmp;

    public ImportReport importFromCsv(MultipartFile file) {
        CSVReader reader = getCSVReaderFromMultipart(file);
        String [] nextLine;
        lineNumber  = 0;
        List<Product> prods = new ArrayList<>();
        importReport = new ImportReport();
        try {
            while ((nextLine = reader.readNext()) != null) {
                if(lineNumber++ < ExportService.PRODUCTS_HEADER_LINES) {
                    continue;
                }
                importReport.addItem();
                try {
                    prods.add(parseProduct(nextLine));
                } catch (ArrayIndexOutOfBoundsException e) {
                    importReport.addError(e.toString(), lineNumber, null);
                }
            }
            importReport.setImportedItems(productService.updateProducts(prods));
        } catch (IOException e) {
            logger.error(e);
            importReport.addError(e.toString(), 0, null);
        }
        if(tmp != null && !tmp.delete()) {
            importReport.addError("Can't delete temporary file", 0, null);
        }
        return importReport;
    }


    private CSVReader getCSVReaderFromMultipart(MultipartFile file) {
        try {
            tmp = new File(file.getOriginalFilename());
            FileOutputStream fos = new FileOutputStream(tmp);
            fos.write(file.getBytes());
            fos.flush();
            fos.close();
            return new CSVReader(new FileReader(tmp), ExportService.CSV_SEPARATOR);
        }
        catch (IOException e) {
            logger.error(e);
            importReport.addError(e.toString(), 0, null);
            return null;
        }
    }

    private Product parseProduct(String[] nextLine) {
        Product prod = new Product();
        setStringProductAttributes(prod, nextLine);
        setPrice(prod, nextLine);
        setCategory(prod, nextLine);
        return prod;
    }

    private void setStringProductAttributes(Product prod, String[] nextLine) throws ArrayIndexOutOfBoundsException {
        prod.setArticul(nextLine[ARTICUL]);
        prod.setTitle(nextLine[TITLE]);
        prod.setPhoto(nextLine[PHOTO]);
        prod.setDescription(nextLine[DESCRIPTION]);
    }

    private void setPrice(Product prod, String[] nextLine)  throws ArrayIndexOutOfBoundsException {
        if(nextLine[PRICE] != null) {
            try {
                prod.setPrice(new BigDecimal(nextLine[PRICE]));
            } catch (NumberFormatException e) {
                importReport.addError(e.toString(), lineNumber, Arrays.toString(nextLine));
                logger.error(e);
            }
        }
    }

    private void setCategory(Product prod, String[] nextLine)  throws ArrayIndexOutOfBoundsException {
        Category category = null;
        if(nextLine[CATEGORY_ID] != null) {
            try {
                Long categoryId = Long.parseLong(nextLine[CATEGORY_ID]);
                category = categoryService.findOne(categoryId);
            } catch (NumberFormatException e) {
                importReport.addError(e.toString(), lineNumber, Arrays.toString(nextLine));
                logger.error(e);
            }
        }
        if(category == null && nextLine[CATEGORY_TITLE] != null) {
            category = categoryService.findByTitle(nextLine[CATEGORY_TITLE]);
        }
        prod.setCategory(category);
    }
}
