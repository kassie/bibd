<%@include file="/WEB-INF/views/utils/include.jsp"%>

<div class="container">

    <c:set value="${profile.cart}" var="cart" />
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><spring:message code="cart" /></h1>
            <ol class="breadcrumb">
                <li><a href="${home}/"><spring:message code="home"/></a></li>
                <li class="active"><spring:message code="cart" /></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <spring:message code="cart.empty" var="cartEmpty" />
            <c:choose>
                <c:when test="${not empty cart.products}">
                    <table class="table table-striped cart">
                        <c:forEach items="${cart.products}" var="product" varStatus="counter">
                            <tr>
                                <td class="td-image">
                                    <a href="${home}/catalog/${product.category.link}/${product.id}" >
                                        <span class="product-image cart-image" style="background-image: url(${product.photo})"></span>
                                    </a>
                                </td>
                                <td><h4><a href="${home}/catalog/${product.category.link}/${product.id}">${product.title}</a></h4></td>
                                <td class="text-right">
                                    <span class="price">
                                        ${profile.currency.sign}
                                        <fmt:formatNumber maxFractionDigits="2" value="${product.price * profile.currency.rateToMain}" />
                                    </span>
                                    <span class="cart-remove">
                                        <span class="item-info-holder" data-product-id="${product.id}" data-price="${product.price}"></span>
                                        <a href="#" class="delete-mark prevent-default"><spring:message code="remove" /></a>
                                    </span>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    <hr />
                    <span class="pull-right total">
                        <spring:message code="cart.total" />:
                        ${profile.currency.sign}
                        <span class="total-price">
                            <fmt:formatNumber maxFractionDigits="2" value="${cart.total * profile.currency.rateToMain}" />
                        </span>
                    </span>
                    <div class="clear"></div>
                    <span class="pull-right">
                        <span class="cart-info-holder" data-user-id="${profile.id}"></span>
                        <button class="btn btn-default"><spring:message code="cart.action.continue-shopping" /></button>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#confirm-checkout">
                            <spring:message code="cart.action.checkout" />
                        </button>
                    </span>
                </c:when>
                <c:otherwise>
                    ${cartEmpty}
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<%--#confirm-checkout--%>
<div class="modal fade"
        id="confirm-checkout"
        tabindex="-1"
        role="dialog"
        aria-labelledby="confirm-checkoutLabel"
        aria-hidden="true">
    <form action="${home}/cart/pay" method="post" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirm-checkoutLabel"><spring:message code="cart.confirmation" /></h4>
            </div>
            <div class="modal-body">
                <spring:message code="cart.choose-payment" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="close" /></button>
                <input type="submit" class="btn btn-primary" value="<spring:message code='cart.pay' />" />
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var userId = $('.cart-info-holder').data('userId');
        $(document).on('click', '.delete-mark', function(e) {
            var $tr = $(this).parents('tr');
            var infoHolder = $tr.find('.item-info-holder');
            var productId = infoHolder.data('productId');
            $.post('${home}/cart/remove', {userId : userId, prodId : productId}, function(data) {
                if(data) {
                    $tr.remove();
                    updateTotalCost();
                    decreaseCartCounter();
                    checkCartEmptiness();
                }
            });
        });

        function updateTotalCost() {
            var prices = $('.item-info-holder');
            var total = 0;
            prices.each(function() {
                var price = parseFloat($(this).data('price'));
                if(price) {
                    total += price;
                }
            });
            $('.total-price').text(total);
        }

        function decreaseCartCounter() {
            var count = parseInt($('.cart-counter').text());
            if(count) {
                $('.cart-counter').text(--count);
            }
        }

        function checkCartEmptiness() {
            var emptyCartMessage = '${cartEmpty}';
            var $cart = $('.cart');
            if(!$cart.find('tr').length) {
                $cart.parent().html(emptyCartMessage);
            }
        }
    });
</script>