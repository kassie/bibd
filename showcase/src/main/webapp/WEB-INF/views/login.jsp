<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <form id="loginForm" class="form-horizontal well" method="POST" action="${home}/login-me">
                <h3 class="text-center">
                    <span><spring:message code="login" /></span>
                </h3>

                <div class="hidden alert alert-error text-center form-group">
                    <strong><spring:message code="error" />:</strong>
                    <c:choose>
                        <c:when test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message == 'Bad credentials'}">
                            <spring:message code="error.bad-credentials" />
                        </c:when>
                    </c:choose>
                </div>
                <div class="form-group">
                    <spring:message code="email" var="email"/>
                    <label class="control-label col-md-3">${email}</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" id="usernameInput" name="username" placeholder="${email}" value="${userEmail}" />
                    </div>
                </div>
                <div class="form-group">
                    <spring:message code="password" var="pass"/>
                    <label class="control-label col-md-3">${pass}</label>
                    <div class="col-md-9">
                        <input class="enterable form-control" type="password" name="password" placeholder="${pass}" />
                        <span><a href="${home}/login/forgot-password"><spring:message code="login.forgot-password" /></a></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <spring:message code="login" var="toLogin" />
                        <input type="submit" class="submitFormBtn btn" value="${toLogin}" />
                        &nbsp;&nbsp;<a href="${home}/registration"><spring:message code="login.register" /></a>
                        <br />
                        <input name="_spring_security_remember_me" type="checkbox" checked="checked" class="check">
                        <span><spring:message code="login.remember-me" /></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>