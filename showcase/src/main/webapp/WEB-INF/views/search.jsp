<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><spring:message code="search" /></h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home"/></a></li>
            <li class="active"><spring:message code="search"/></li>
        </ol>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <c:if test="${not empty categories}">
                <h3><spring:message code="category.plural"/></h3>
                <table class="table table-hover table-condensed table-striped">
                    <tr>
                        <th><spring:message code="title" /></th>
                        <c:forEach items="${categories}" var="category">
                            <tr>
                                <td><a hre="${home}/catalog/${category.link}">${category.title}</a></td>
                            </tr>
                        </c:forEach>
                    </tr>
                </table>
            </c:if>
            <c:if test="${not empty products}">
                <h3><spring:message code="product.plural"/></h3>
                <table class="table table-hover table-condensed table-striped">
                    <tr>
                        <th><spring:message code="title" /></th>
                        <th><spring:message code="price" /></th>
                        <th><spring:message code="category" /></th>
                    </tr>
                    <c:forEach items="${products}" var="product">
                        <tr>
                            <td><a href="${home}/catalog/${product.category.link}/${product.id}">${product.title}</a></td>
                            <td><b>$ <fmt:formatNumber value="${product.price}"/></b></td>
                            <td><a href="${home}/catalog/${product.category.link}">${product.title}</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <c:if test="${empty categories && empty products}">
                <h4><spring:message code="search.nothing-found" /></h4>
            </c:if>
        </div>
    </div>
</div>