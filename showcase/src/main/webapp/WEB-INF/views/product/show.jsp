<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">${product.title}</h1>
            <ol class="breadcrumb">
                <li><a href="${home}/"><spring:message code="home"/></a></li>
                <li><a href="${home}/catalog"><spring:message code="catalog"/></a></li>
                <li><a href="${home}/catalog/${product.category.link}">${product.category.title}</a></li>
                <li class="active">${product.title}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <img class="img-responsive img-thumbnail img-left" src="${product.photo}">
            <h3>${product.title}</h3>
            <security:authorize access="isAuthenticated()">
                <hr />
                <span class="info-holder" data-price="${product.price}" data-product-id="${product.id}" data-user-id="${profile.id}"></span>
                <span class="price"><spring:message code="price"/>:
                    ${profile.currency.sign}
                    <fmt:formatNumber maxFractionDigits="2" value="${product.price * profile.currency.rateToMain}" />
                </span>
                <c:if test="${product.inCart}">
                    <c:set value="show-in-cart" var="showInCart" />
                </c:if>
                <a href="${home}/cart" class="btn btn-warning in-cart ${showInCart}"><spring:message code="cart.in-cart" /></a>
                <button class="btn btn-success add-to-cart ${showInCart}"><spring:message code="cart.add" /></button>
                <hr />
            </security:authorize>
            <p>${product.description}</p>
        </div>
    </div>

</div><!-- /.container -->

<script type="text/javascript">
    $(document).ready(function() {
        $('.add-to-cart').click(function() {
            var $infoHolder = $('.info-holder');
            var userId = $infoHolder.data('userId');
            var prodId = $infoHolder.data('productId');
            $.post('${home}/cart/add', {
                    userId : userId,
                    prodId : prodId
                }, function(data) {
                    if(data) {
                        $('.cart-counter').text(data);
                        $('.add-to-cart').toggle();
                        $('.in-cart').toggle();
                    }
             });
        });
    });
</script>