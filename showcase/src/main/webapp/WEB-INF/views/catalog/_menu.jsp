<%@ include file="/WEB-INF/views/utils/include.jsp" %>


<ul class="nav nav-stacked nav-pills">
    <li>
        <h3><spring:message code="category.plural" /></h3>
    </li>
    <hr />
    <c:forEach items="${categories}" var="category">
        <li><a href="${home}/catalog/${category.link}">${category.title}</a></li>
    </c:forEach>
</ul>

<script type="text/javascript">
    $(document).ready(function() {
        var url = document.location.href;
        $('.main-cat a').each(function() {
            console.log($(this).attr('href'));
            if(new RegExp($(this).attr('href'), 'g', 'i').test(url)) {
                $(this).addClass('active');
            }
        });
    });
</script>