<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<div class="container">

    <hr>

    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>&copy; <spring:message code="copyright"/></p>
            </div>
        </div>
    </footer>

</div><!-- /.container -->