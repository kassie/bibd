<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- You'll want to use a responsive image option so this logo looks good on devices - I recommend using something like retina.js (do a quick Google search for it and you'll find it) -->
            <a class="navbar-brand" href="${home}/"><spring:message code="app.title" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <form class="navbar-form navbar-left"
                    role="search"
                    action="${home}/search">
                <div class="form-group">
                    <input type="text" name="query" class="form-control" placeholder="<spring:message code='search' />">
                </div>
                <button type="submit" class="btn btn-default"><spring:message code="search.do" /></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${home}/catalog"><spring:message code="catalog" /></a></li>
                <security:authorize access="isAuthenticated()">
                    <li><a href="${home}/cart"><spring:message code="cart" />&nbsp;
                        <span class="badge cart-counter">${profile.cart.count}</span>
                    </a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">${profile.name}&nbsp;${profile.surname} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                    <li><a href="${home}/users/${profile.id}"><spring:message code="user.profile" /></a></li>
                                    <li><a href="${home}/logout"><spring:message code="logout" /></a></li>
                            </ul>
                    </li>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <li>
                        <a href="${home}/login"><spring:message code="login" /></a>
                    </li>
                </security:authorize>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>