<%@ include file="/WEB-INF/views/utils/include.jsp"%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><spring:message code="user.profile" /></h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home"/></a></li>
            <li class="active"><spring:message code="user.profile" /></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#profile" data-toggle="tab"><spring:message code="user.profile" /></a></li>
            <li><a href="#history" data-toggle="tab"><spring:message code="user.purchase-history" /></a></li>
        </ul>
    </div>

    <%--user profile--%>
    <div class="col-md-9 tab-content">
        <div class="tab-pane fade in active" id="profile">
            <form:form
                    class="form-horizontal"
                    action="${home}/users/update"
                    commandName="user"
                    method="post">
                <div class="form-group">
                    <spring:message code="user.name" var="userName"/>
                    <form:label path="name" cssClass="control-label col-md-2">${userName}</form:label>
                    <div class="col-md-8">
                        <form:input path="name" cssClass="form-control" placeholder="${userName}" />
                        <form:errors path="name" />
                    </div>
                </div>
                <div class="form-group">
                    <spring:message code="user.surname" var="userSurname"/>
                    <form:label path="surname" cssClass="control-label col-md-2">${userSurname}</form:label>
                    <div class="col-md-8">
                        <form:input path="surname" cssClass="form-control" placeholder="${userSurname}" />
                        <form:errors path="surname" />
                    </div>
                </div>
                <div class="form-group">
                    <spring:message code="email" var="userEmail"/>
                    <form:label path="email" cssClass="control-label col-md-2">${userEmail}</form:label>
                    <div class="col-md-8">
                        <form:input path="email" cssClass="form-control" placeholder="${userEmail}" />
                        <form:errors path="email" />
                    </div>
                </div>
                <div class="form-group">
                    <spring:message code="user.phone" var="phone"/>
                    <form:label path="phone" cssClass="control-label col-md-2">${phone}</form:label>
                    <div class="col-md-8">
                        <form:input path="phone" cssClass="form-control" placeholder="${phone}" />
                        <form:errors path="phone" />
                    </div>
                </div>
                <div class="form-group">
                    <spring:message code="user.address" var="address"/>
                    <form:label path="address" cssClass="control-label col-md-2">${address}</form:label>
                    <div class="col-md-8">
                        <form:input path="address" cssClass="form-control" placeholder="${address}" />
                        <form:errors path="address" />
                    </div>
                </div>
                <div class="form-group">
                    <spring:message code="user.currency" var="curr"/>
                    <label class="control-label col-md-2">${curr}</label>
                    <div class="col-md-8">
                        <select class="form-control" name="currId" id="currencySelect">
                            <c:forEach items="${currencies}" var="item">
                                <c:set var="selected" value="" />
                                <c:if test="${item.id == user.currency.id}" >
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option ${selected} value="${item.id}">${item.title}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-8">
                        <form:hidden path="id" />
                        <spring:message code="save" var="save" />
                        <input class="btn btn-primary" type="submit" value="${save}" />
                    </div>
                </div>
            </form:form>
        </div>

        <%--user purchase history--%>
        <div class="tab-pane fade" id="history">
            <c:choose>
                <c:when test="${not empty purchases}">
                    <div class="panel-group" id="accordion">
                        <c:forEach items="${purchases}" var="purchase" varStatus="counter">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse${counter.count}">
                                            <span class="bold">${profile.currency.sign}
                                                <fmt:formatNumber maxFractionDigits="2" value="${purchase.total * profile.currency.rateToMain}" />&nbsp;&mdash;&nbsp;
                                                <fmt:formatDate value="${purchase.purchased}" pattern="dd:MM:yyyy hh:mm" />
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse${counter.count}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table table-hover">
                                            <tr>
                                                <th>#</th>
                                                <th><spring:message code="title" /></th>
                                                <th><spring:message code="price" /></th>
                                            </tr>
                                            <c:forEach items="${purchase.items}" var="item" varStatus="itemCounter">
                                                <tr>
                                                    <td>${itemCounter.count}</td>
                                                    <td><a href="${home}/catalog/products/${item.productId}">${item.title}</a></td>
                                                    <td>${profile.currency.sign} <fmt:formatNumber maxFractionDigits="2" value="${item.price  * profile.currency.rateToMain}" /></td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                        <button class="btn btn-success save-history pull-right" data-id="${purchase.id}">
                                            <spring:message code="save" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </c:when>
                <c:otherwise>
                    <spring:message code="user.purchase-history.empty" />
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.save-history').click(function() {
            var id = $(this).data('id');
            window.location.href = "${home}/users/export?id=" + id;
        });
    });
</script>