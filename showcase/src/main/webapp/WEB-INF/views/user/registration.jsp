<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="container">
    <div class="row">
        <h3 class="well"><spring:message code="user.registration" /> </h3>
        <form:form method="POST"
                   cssClass="form-horizontal"
                   commandName="user"
                   action="${home}/registration">
            <div class="form-group">
                <spring:message code="email" var="email" />
                <label class="control-label col-md-3">${email}</label>
                <div class="col-md-9">
                    <form:input path="email" cssClass="required-field form-control" placeholder="${email}" />
                    <form:errors path="email" cssClass="error-help" />
                </div>
            </div>
            <div class="form-group">
                <spring:message code="password" var="pass" />
                <label class="control-label col-md-3">${pass}</label>
                <div class="col-md-9">
                    <input class="form-control" type="password" name="pass" placeholder="${pass}" />
                </div>
            </div>
            <div class="form-group">
                <spring:message code="user.retype-pass" var="retypePass" />
                <label class="control-label col-md-3">${retypePass}</label>
                <div class="col-md-9">
                    <input class="form-control" type="password" name="rePass" placeholder="${retypePass}" />
                </div>
            </div>
            <div class="col-md-6 col-md-offset-3 center-text">
                    <spring:message code="user.register" var="register" />
                    <input type="submit" class="btn btn-large btn-success" value="${register}" />
            </div>
        </form:form>
    </div>
</div>