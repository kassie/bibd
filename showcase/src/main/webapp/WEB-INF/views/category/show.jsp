<%@ include file="/WEB-INF/views/utils/include.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">${category.title}</h1>
        <ol class="breadcrumb">
            <li><a href="${home}/"><spring:message code="home"/></a></li>
            <li><a href="${home}/catalog"><spring:message code="catalog"/></a></li>
            <li class="active">${category.title}</li>
        </ol>
    </div>
</div>

<c:forEach var="product" items="${page.content}">
    <div class="row">
        <div class="col-md-5">
            <a class="product-image category-image" href="${home}/catalog/${category.link}/${product.id}" style="background-image: url(${product.photo})">
                <%--<img class="img-responsive" src="${product.photo}" title="${product.title}">--%>
            </a>
            <security:authorize access="isAnonymous()">
                <span class="price category-price"><spring:message code="price"/>:
                    $
                    <fmt:formatNumber maxFractionDigits="2" value="${product.price}" />
                </span>
            </security:authorize>
            <security:authorize access="isAuthenticated()">
                <span class="price category-price"><spring:message code="price"/>:
                    ${profile.currency.sign}
                    <fmt:formatNumber maxFractionDigits="2" value="${product.price * profile.currency.rateToMain}" />
                </span>
            </security:authorize>
            <a class="btn btn-primary btn-go" href="${home}/catalog/${category.link}/${product.id}">
                <spring:message code="go"/>&nbsp;<i class="fa fa-angle-right"></i>
            </a>
        </div>
        <div class="col-md-7">
            <h3><a href="${home}/catalog/${category.link}/${product.id}">${product.title}</a></h3>
            <p>${fn:substring(product.description, 0, 512)}...</p>
        </div>
    </div>
    <hr>
</c:forEach>

<div class="row text-center">
    <div class="col-lg-12">
        <%@ include file="/WEB-INF/views/utils/_pagination.jsp" %>
    </div>
</div>