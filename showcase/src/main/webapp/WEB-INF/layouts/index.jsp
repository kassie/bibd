<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="eshop"/>
        <meta name="author" content="Leonid Sazankov"/>
        <title><spring:message code="title"/></title>
        <link type="image/x-icon" rel="shortcut icon" href="/favicon.ico"/>
        <link href="${home}/resources/assets/css/index.css" rel="stylesheet" />
    </head>
    <body>
        <div id="main-page">
            <div class="abs-button">
                <a href="${home}/catalog">Welcome</a>
            </div>
        </div>
    </body>
</html>
