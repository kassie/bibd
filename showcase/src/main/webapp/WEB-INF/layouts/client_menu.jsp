<%@ include file="/WEB-INF/views/utils/include.jsp" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="eshop" />
    <meta name="author" content="Leonid Sazankov" />

    <title><spring:message code="title" /></title>

    <link type="image/x-icon" rel="shortcut icon" href="/favicon.ico" />

    <!-- Bootstrap core CSS -->
    <link href="${home}/resources/assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Custom CSS here -->
    <link href="${home}/resources/assets/css/modern-business.css" rel="stylesheet" />
    <link href="${home}/resources/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

      <!-- JavaScript -->
      <script src="${home}/resources/assets/js/jquery-1.10.2.js"></script>
      <script src="${home}/resources/assets/js/bootstrap.js"></script>
      <script src="${home}/resources/assets/js/modern-business.js"></script>
  </head>

  <body>

    <tiles:insertAttribute name="header" />
    <div class="container">
        <div class="row">
            <div class="col-md-3 sidebar main-cat">
                <tiles:insertAttribute name="menu" />
            </div>
            <div class="col-md-9">
                <tiles:insertAttribute name="body" />
            </div>
        </div>
    </div>
    <tiles:insertAttribute name="footer" />

  </body>
</html>
