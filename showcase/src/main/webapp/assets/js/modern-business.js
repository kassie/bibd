// Activates the Carousel
$('.carousel').carousel({
  interval: 5000
})

// Activates Tooltips for Social Links
$('.tooltip-social').tooltip({
  selector: "a[data-toggle=tooltip]"
})

$(document).ready(function() {
     $('.prevent-default').click(function(e) {
         e.preventDefault();
     });
}) ;

//flash message
var flashMessageSelector = '.flash-message';
var flashErrorSelector = '.flash-error';
var flashSuccessSelector = '.flash-success';
$(document).on('click', flashMessageSelector, function() {
    $(this).fadeOut(200);
});

var FLASH_MESSAGE = {
    success : 'flashSuccessMessage',
    error : 'flashErrorMessage'
};

function displayFlashMessage(message) {
    if(message) {
        if(message.key == FLASH_MESSAGE.success) {
            if(!message.message) { message.message = "Done"; }
            $(flashSuccessSelector).html('<span>' + message.message + '</span>');
            $(flashSuccessSelector).fadeIn(200);
        } else if(message.key == FLASH_MESSAGE.error) {
            if(!message.message) { message.message = "Error"; }
            $(flashErrorSelector).html('<span>' + message.message + '</span>');
            $(flashErrorSelector).fadeIn(200);
        }
        setTimeout(function() {
                $(flashMessageSelector).fadeOut(400);
            },
            3500
        )
    }
}