package by.bsuir.bibd.util;

import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.model.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserPopulationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserService userService;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if(modelAndView != null) {
            User user = null;
            Object userObject = modelAndView.getModel().get("profile");
            if(userObject != null) {
                user = (User) userObject;
            } else {
                user = userService.findOne(request);
                modelAndView.addObject("profile", user);
            }
        }
    }

}