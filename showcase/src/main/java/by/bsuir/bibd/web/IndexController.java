package by.bsuir.bibd.web;

import by.bsuir.bibd.model.category.Category;
import by.bsuir.bibd.model.category.CategoryService;
import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showIndex(Model model) {
        return "index";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String doSearch(Model model, @RequestParam String query) {
        if(query == null) {
            return "redirect:/";
        }
        List<Product> products = productService.findByTitleContaining(query);
        for(Product p : products) {
            p.setTitle(StringUtil.bold(p.getTitle(), query));
        }
        model.addAttribute("products", products);
        List<Category> categories = categoryService.findByTitleContaining(query);
        for(Category c : categories) {
            c.setTitle(StringUtil.bold(c.getTitle(), query));
        }
        model.addAttribute("categories", categories);
        return "client:search";
    }

}
