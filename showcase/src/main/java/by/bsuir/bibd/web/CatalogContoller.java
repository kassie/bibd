package by.bsuir.bibd.web;

import by.bsuir.bibd.model.category.Category;
import by.bsuir.bibd.model.category.CategoryService;
import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.model.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CatalogContoller {

    @Value("${product.showcase.items-on-page}")
    private Integer itemsOnPage;

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String showIndex(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        return "client_menu:catalog/index";
    }

    @RequestMapping(value = "/catalog/{categoryLink}", method = RequestMethod.GET)
    public String showCategory(@PathVariable String categoryLink, Model model,
            @RequestParam(required = false) Integer page) {
        Category category = categoryService.findByLink(categoryLink);
        if(category == null) {
            return "client:error/404";
        }
        model.addAttribute("page", productService.findByCategory(category.getId(), page, itemsOnPage));
        model.addAttribute("category", category);
        model.addAttribute("categories", categoryService.findAll());
        return "client_menu:category/show";
    }

    @RequestMapping(value = "/catalog/{categoryLink}/{productId}", method = RequestMethod.GET)
    public String showProduct(@PathVariable String categoryLink, @PathVariable Long productId,
            Model model, HttpServletRequest req) {
        Product product = productService.findOne(productId);
        if(categoryService.findByLink(categoryLink) == null || product == null || !product.isActive()) {
            return "client:error/404";
        }
        model.addAttribute("product", productService.setInCart(req, product));
        return "client:product/show";
    }

    @RequestMapping(value = "/catalog/products/{productId}", method = RequestMethod.GET)
    public String showProductById(@PathVariable Long productId,
                              Model model, HttpServletRequest req) {
        Product product = productService.findOne(productId);
        if(product == null || !product.isActive()) {
            return "client:error/404";
        }
        model.addAttribute("product", productService.setInCart(req, product));
        return "client:product/show";
    }

}