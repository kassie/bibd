package by.bsuir.bibd.web;

import by.bsuir.bibd.model.cart.Cart;
import by.bsuir.bibd.model.cart.CartService;
import by.bsuir.bibd.model.product.Product;
import by.bsuir.bibd.model.product.ProductService;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItemService;
import by.bsuir.bibd.model.purchase.PurchaseService;
import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.model.user.UserService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@Controller
public class CartController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private CartService cartService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PurchaseItemService purchaseItemService;

    @RequestMapping(value = "/cart/add", method = RequestMethod.POST)
    @ResponseBody
    public int addToCart(@RequestParam("userId") Long userId, @RequestParam("prodId") Long prodId,
            HttpServletRequest req) {
        User customer = userService.findOne(userId);
        User authorizedUser = userService.findOne(req);
        Product product = productService.findOne(prodId);
        if(customer != null && authorizedUser != null && customer.equals(authorizedUser)
                && product != null && product.isActive()) {
            Cart cart = customer.getCart();
            Set<Product> products = cart.getProducts();
            if(products.add(product)) {
                cart.setProducts(products);
                cartService.update(cart);
                return products.size();
            }
        }
        return 0;
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String showCart() {
        return "client:cart/show";
    }

    @RequestMapping(value = "/cart/remove", method = RequestMethod.POST)
    @ResponseBody
    public boolean removeFromCart(@RequestParam("userId") Long userId, @RequestParam("prodId") Long prodId,
            HttpServletRequest req) {
        User customer = userService.findOne(userId);
        User authorizedUser = userService.findOne(req);
        Product product = productService.findOne(prodId);
        if(customer != null && authorizedUser != null && customer.equals(authorizedUser)
                && product != null) {
            Cart cart = customer.getCart();
            Set<Product> products = cart.getProducts();
            if(products.remove(product)) {
                cart.setProducts(products);
                cartService.update(cart);
                return true;
            }
        }
        return false;
    }

    @RequestMapping(value = "/cart/pay", method = RequestMethod.POST)
    public String acceptPayment(HttpServletRequest req, RedirectAttributes redirectAttributes) {
        User customer = userService.findOne(req);
        if(customer == null) {
            return "client:error/404";
        }
        Cart cart = customer.getCart();
        if(cart == null || cart.getProducts().isEmpty()) {
            redirectAttributes.addFlashAttribute(FlashMessage.ERROR_KEY, FlashMessage.error("cart.error.empty"));
            return "redirect:/cart";
        }
        purchaseService.create(cart, customer);
        cartService.clean(cart);
        redirectAttributes.addFlashAttribute(FlashMessage.SUCCESS_KEY, FlashMessage.success("cart.success.paid"));
        return "redirect:/cart";
    }
}
