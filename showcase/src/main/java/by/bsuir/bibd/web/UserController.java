package by.bsuir.bibd.web;

import by.bsuir.bibd.model.currency.CurrencyService;
import by.bsuir.bibd.model.purchase.Purchase;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItem;
import by.bsuir.bibd.model.purchase.PurchaseItem.PurchaseItemService;
import by.bsuir.bibd.model.purchase.PurchaseService;
import by.bsuir.bibd.model.user.User;
import by.bsuir.bibd.model.user.UserService;
import by.bsuir.bibd.service.export.PurchaseExportService;
import by.bsuir.bibd.util.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PurchaseItemService purchaseItemService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginShow() {
        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registerShow(Model model) {
        model.addAttribute("user", new User());
        return "client:user/registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute @Valid User user, BindingResult bindingResult,
            Model model, @RequestParam("pass") String pass, @RequestParam("rePass") String rePass,
            RedirectAttributes redirectAttributes) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return "client:user/registration";
        }
        if(!userService.validRegistrationData(model, user, pass, rePass)) {
            model.addAttribute("user", user);
            return "client:user/registration";
        }
        user.setPassword(pass);
        user = userService.create(user);
        redirectAttributes.addFlashAttribute("user", user);
        return "redirect:/login";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String showProfile(Model model, @PathVariable Long id, HttpServletRequest req) {
        User user = userService.findOne(id);
        if(user == null || !user.equals(userService.findOne(req))) {
            return "404";
        }
        populateModel(model, user);
        return "client:user/show";
    }

    @RequestMapping(value = "/users/update", method = RequestMethod.POST)
    public String update(@ModelAttribute @Valid User user, RedirectAttributes redirectAttributes,
            Model model, BindingResult bindingResult, HttpServletRequest req, @RequestParam Long currId) {
        if(bindingResult.hasErrors()) {
            populateModel(model, user);
            return "client:user/show";
        }
        if(!user.getId().equals(userService.findOne(req).getId())) {
            return "404";
        }
        user.setCurrency(currencyService.findOne(currId));
        FlashMessage result = userService.update(user);
        redirectAttributes.addFlashAttribute(result.getKey(), result.getCode());
        return "redirect:/users/" + user.getId();
    }

    @RequestMapping(value = "/users/export", method = RequestMethod.GET)
    private void exportPurchases(@RequestParam Long id, HttpServletRequest req, HttpServletResponse res) {
        User customer = userService.findOne(req);
        if(customer == null || id == null) {
//            return FlashMessage.error("user.not-found");
        }
        Purchase purchase = purchaseService.findOne(id);
        List<PurchaseItem> items = purchaseItemService.findByPurchaseId(id);
        if(items.isEmpty()) {
//            return FlashMessage.error("user.purchase-history.empty");
        }
        purchase.setItems(items);
        purchase.setCurrency(customer.getCurrency());
        try(OutputStream os = res.getOutputStream()) {
            res.setContentType("text/csv;charset=UTF-8");
            res.setHeader("Content-disposition", "attachment;filename=doc.csv");
            new PurchaseExportService(purchase).export(os);
            os.flush();
//            return FlashMessage.success("success");
        } catch (IOException e) {
//            return FlashMessage.error("");
        }
    }

    private void populateModel(Model model, User user) {
        model.addAttribute("user", user);
        model.addAttribute("currencies", currencyService.findAll());
        List<Purchase> purchases = purchaseService.findByCustomerId(user.getId());
        for(Purchase p : purchases) {
            p.setItems(purchaseItemService.findByPurchaseId(p.getId()));
        }
        model.addAttribute("purchases", purchases);
    }

}
